# test-generator README


## Features

This extension allows you to generate automatically some test structure to help you cover that
technical debt in case you didn't do your unit tests before.

Is intended to work for angular components but you might also try it with other typescript files and
let us know what happens.


## Release Notes
This extensión is only a beta so we are still developing and bugfixing

## Run a tool
From the Run menu in Visual Studio Code, as shown in the image, or by pressing F5.
![alt text](./images/Run-Test-Generator.png)
## Operate the tool
We can use three options configured in the tool: file unit test, unit tests of the entire project, unit test of a section of the component (function or method).
### File unit test
We open an Angular project. As shown in the image, right-click on a component that we want to test, and click on Test Generator: This File.
![alt text](./images/Test-Generator_This-File.png)
In the same way, if we right-click inside an artifact or file open in the current editor, the option "Test Generator: This File" appears.
![alt text](./images/Desde-Fichero.png)
### Current selection File
we select a method or function within a component, right-click in Test Generator:current file, will appear, generate a unit test for that method.
![alt text](./images/Current-selection.png)
Note: If the function or method already exists in the test file, it will be overwritten and remain in the same position. If it does not exist, it will appear at the end of the unit test file.
### All files
To generate unit tests for all the files in the project, we have to use ctrl+shift+P and apply the command shown in the image "Test Generator: all typescript files"
![alt text](./images/all-files.png)
## Another way to operate the tool
We can also run a file unit test by opening the commands in the top bar of the environment or by pressing CTRL+Shift+P.
The image you provided shows the commands for the three methods we discussed earlier.
![alt text](./images/ctrl-ship-P.png)
1. Test Generator: all typescript files->unit tests of all the files in the project
2. Test Generator: current selection->unit test of a selection of a file
3. Test Generator: this file->unit tests of a single file

Note: The generation time will vary depending on the number of tests to generate