import { ClassDeclaration, SourceFile } from 'ts-morph';

import { SourceProject } from '../model/SourceProject';
import {
  allInput,
  contenidoElementoImportado,
  elementosInput,
  mocksElementoImportado,
  typosInput,
} from './funcionesAst';

// funciones para el valor de las propiedades segun typado.
export function isTypeBoolean(typado: string) {
  let resultado: boolean | undefined;
  if (typado === 'boolean') {
    resultado = true;
  }
  return resultado;
}
export function isTypeString(typado: string, propiedad: string) {
  let resultado: string | undefined;
  if (typado === 'string') {
    resultado = `'${propiedad.replace(':', '')}'`;
  }
  return resultado;
}
export function isTypeNumber(typado: string) {
  let resultado: number | undefined;
  if (typado === 'number') {
    resultado = 1;
  }
  return resultado;
}
export function isTypeArray(typado: string) {
  let resultado: string | undefined;
  if (typado.startsWith('import')) {
    typado = typado.replace(/import(.+)\)./gm, '');
    if (typado.match(/\[\]/gm)) {
      resultado = '[]';
    }
  } else {
    if (typado.match(/\[\]/gm)) {
      resultado = '[]';
    }
  }
  return resultado;
}
export function isTypeObject(typado: string) {
  let resultado: string | undefined;
  if (typado.startsWith('import')) {
    resultado = `{} as any`;
  } else if (typado.match(/^[A-Z]/gm)) {
    resultado = `{}as any`;
  }
  return resultado;
}
export function isTypeObjectArray(typado: string, texto: string) {
  let resultado = '';
  const objetos: string[] = [];
  const expresionRegular = /(\w+)\[\]/g;
  if (expresionRegular.test(typado)) {
    resultado = ` [\n{\n`;
    typado = typado.replace(/\[\]/gs, '');
    objetos.push(typado);
    const buscarElemetoObjeto = mocksElementoImportado(texto, objetos);
    const typed = buscarElemetoObjeto.match(/:\s(\w+)(.+);/g);
    const propiedades = buscarElemetoObjeto.match(/(\w+):/gs);
    let i = 0;
    for (const propiedad of propiedades) {
      resultado += `\n${propiedad}`;
      typed[i] = typed[i].replace(/:\s+/gs, '').replace(';', '');
      if (isTypeBoolean(typado[i])) {
        resultado += ` ${isTypeBoolean(typed[i])},`;
      } else if (isTypeNumber(typed[i])) {
        resultado += ` ${isTypeNumber(typed[i])},`;
      } else if (isTypeString(typed[i], propiedad)) {
        resultado += ` ${isTypeString(typed[i], propiedad)},`;
      } else if (isTypeObjectArray(typed[i], texto)) {
        resultado += `${isTypeObjectArray(typed[i], texto)}`;
      }
      i++;
    }
    resultado += `\n},\n],`;
  }
  return resultado;
}
//logica para sacar el nombre de interface o clas del componente que se esta importando en el componente del test
export function nombreVariable(texto: string) {
  const matches = texto.match(/(\w+)\s+{/gs);
  let nombreInterface = '';
  if (matches) {
    nombreInterface = matches[0];
    nombreInterface = nombreInterface.replace(/\s+\{/gs, '');
  }
  return nombreInterface;
}

//Estos son los elementos a apartir de aqui que importamos en los otros archivos
export function mocksElementoImportados(
  clase: ClassDeclaration,
  input: string,
  sourcefile: SourceFile,
  sourceProject: SourceProject
) {
  const all = allInput(clase);
  let resultado = '';
  const elementosInputClase = typosInput(all);
  const archivoOtroComponent = contenidoElementoImportado(sourcefile, elementosInputClase, input, sourceProject);
  if (archivoOtroComponent) {
    const texto = mocksElementoImportado(archivoOtroComponent, elementosInputClase);
    if (texto) {
      resultado = `const mock${nombreVariable(texto)} = [\n\t{`;
      const typado = texto.match(/:\s(\w+)(.+);/g);
      const propiedades = texto.match(/(\w+):/gs);
      let i = 0;
      for (const propiedad of propiedades) {
        resultado += `\n\t\t${propiedad}`;
        typado[i] = typado[i].replace(/:\s+/gs, '').replace(';', '');
        if (isTypeBoolean(typado[i])) {
          resultado += ` ${isTypeBoolean(typado[i])},`;
        } else if (isTypeNumber(typado[i])) {
          resultado += ` ${isTypeNumber(typado[i])},`;
        } else if (isTypeString(typado[i], propiedad)) {
          resultado += ` ${isTypeString(typado[i], propiedad)},`;
        } else if (isTypeObjectArray(typado[i], archivoOtroComponent)) {
          resultado += `${isTypeObjectArray(typado[i], archivoOtroComponent)}`;
        }
        i++;
      }
      resultado += `\n\t},\n];`;
    }
    return resultado;
  }
  
}

//nombre de la clase o interface que lo usamos como nombre de la variable para el const
export function namedVariable(
  clase: ClassDeclaration,
  input: string,
  sourcefile: SourceFile,
  sourceProject: SourceProject
) {
  const all = allInput(clase);
  const elementosInputClase = typosInput(all);
  const archivoOtroComponent = contenidoElementoImportado(sourcefile, elementosInputClase, input, sourceProject) ?? '';
  const texto = mocksElementoImportado(archivoOtroComponent, elementosInputClase);
  return nombreVariable(texto);
}
//nombre de la variable @input si coincide con una clase
export function namedElementInput(clase: ClassDeclaration) {
  const all = allInput(clase);
  const elementosInputClase = elementosInput(all);
  const typosElementosClase = typosInput(all);
  let i = 0;
  const expresionRegular = /^[A-Z](\w+)/gs;
  for (const namedElement of typosElementosClase) {
    if (expresionRegular.test(namedElement)) {
      //console.log(namedElement);
      return elementosInputClase[i];
    }
    i++;
  }
}
