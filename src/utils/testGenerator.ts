import { StandardizedFilePath } from '@ts-morph/common';
import * as fs from 'fs';
import { ClassDeclaration, FunctionDeclaration, MethodDeclaration, Project, SourceFile, ts } from 'ts-morph';
import { SpyOnGenerator } from '../generators/SpyOnGenerator';
import { SourceProject } from '../model/SourceProject';
import { TestGeneratorContext } from '../model/model';
import { TemplateBeforeEach } from '../templates/TeplateBeforeEach';
import { TeplateDescribe } from '../templates/TeplateDescribe';
import { TemplateIt } from '../templates/TeplateIt';
import { TemplateTestbed } from '../templates/TeplateTestbed';
import { mastest } from './condicionales/funcionesCondicionales';
import { añadirImportEnum } from './constants';
import {
  anadirModulos,
  dependencyLibraryTesting,
  elementProviders,
  extraerClase,
  extraerFuncionesGlobales,
  extraerMetodosClase,
  extraerPopiedadesClase,
  extraerPopiedadesClaseName,
  importFunction,
  importTypeAlias,
  modulosImportados,
  parametrosNombre,
  propertiesCalculated,
  rutaProyecto,
  sacarImports
} from './funcionesAst';
import {
  construccionObjeto,
  convertirMayusEnMinus,
  mocksComponentPropiedad,
  mocksGlobales,
  windowsConstruccionObjecto
} from './funcionesMocks';
import { mocksElementoImportados, namedElementInput, namedVariable } from './funcionesMocksElementImports';
import { objetoParametroMetodo } from './funcionesParametrosDeMetodos';
import { generateMethodCall } from './funcionesSpyOn';
export function safeEmptyStr(item: string, suffix: string = ''): string {
  if (item === null || item === undefined) {
    return '';
  }
  return `${item}${suffix}`;
}



function itShouldCreate(variableName: string): TemplateIt {
  const templateIt = new TemplateIt();
  templateIt.testName = 'should create';
  templateIt.source = `expect(${variableName}).toBeTruthy();`;
  return templateIt;
}

export function procesarClase(clase: ClassDeclaration) {
  let extracionClase = '';

  const propiedadesClase = extraerPopiedadesClase(clase);
  extracionClase += 'class ' + clase.getName() + '{\npropiedades de clase:\n';
  for (const propiedadClase of propiedadesClase) {
    extracionClase +=
      '\t' + propiedadClase.visibilidad + ' nombre ' + propiedadClase.cname + ' : ' + propiedadClase.tipo + '\n';
  }
  extracionClase += '\n\t' + 'metodos de clase:' + '\n';
}

export function procesarMetodo(
  metodo: MethodDeclaration,
  clase: ClassDeclaration,
  sourceFilesTodo: SourceFile,
  tgcontent: TestGeneratorContext
) {
  const templateIt = new TemplateIt();
  templateIt.testName = metodo.getName();
  templateIt.isAsinc = metodo.isAsync();
  
  const nombresP = extraerPopiedadesClaseName(clase);

  const parametrosN = parametrosNombre(metodo);
  const contenidoMetodo = metodo.getBody()?.getText() ?? '';
  let testSource = '';

  const mocksComponentP = mocksComponentPropiedad(clase, nombresP, contenidoMetodo, sourceFilesTodo?.getText(), metodo);
  if (mocksComponentP !== undefined) {
    testSource += '\n' + mocksComponentP;
  }
  //llamo a la funcion objetoParametroMetodo para deglosar en una constate un objeto segun los parametros que tenga ese metodo
  let variablesObjetoParams = ' ' + objetoParametroMetodo(metodo, sourceFilesTodo);
  //si no teneemos parametros para este metodo , no sacamos la variable con el objeto y solo sacamaos el componente en null
  if (variablesObjetoParams !== undefined) {
    testSource += variablesObjetoParams;
  }
  
  const spyOnGenerator = new SpyOnGenerator(); 
  testSource += '\n' + spyOnGenerator.getSpyOnForMethod(metodo, tgcontent, clase).join('\n');

  testSource += metodo.isAsync() ? '\nawait ' : '\n';
  if (parametrosN?.length) {
    testSource += generateMethodCall(clase, metodo, parametrosN);
  } else {
    testSource += generateMethodCall(clase, metodo);
  }
  templateIt.source = testSource;
  return templateIt;
}

function addTextControlAwaitPromise(funcion: MethodDeclaration | FunctionDeclaration): string {
  if (funcion.isAsync()) {
    return `await `;
  } else {
    return '';
  }
}

export function analysisFileJson(propertySearch: string): StandardizedFilePath[] {
  const arrayFilesJson: SourceFile[] = filesListJson();
  const arrayFilesIsObject: StandardizedFilePath[] = [];
  for (const sourceFileJson of arrayFilesJson) {
    const mock = JSON.parse(sourceFileJson.getText());
    if (mock?.[propertySearch] !== undefined) {
      arrayFilesIsObject.push(sourceFileJson.getFilePath());
    }
  }
  return arrayFilesIsObject;
}

function filesListJson(): SourceFile[] {
  const project: Project = new Project();
  project.addSourceFilesAtPaths(rutaProyecto() + '/test/**/*.json');
  const sourceFiles: SourceFile[] = project.getSourceFiles();
  return sourceFiles;
}

export function aliasModuleExport(sourceFile: SourceFile): string {
  let result: string = '';
  if (sourceFile?.getFilePath().endsWith('services.ts')) {
    const baseName: string = sourceFile.getBaseName();
    const splitBaseName: string[] = baseName.split('.');
    let nameService: string = '';
    splitBaseName.map(bName => {
      if (bName !== 'ts') {
        nameService += bName;
      }
    });
    result = nameService;
  }
  return result;
}

function functionsInsideService(sourceFile: SourceFile, funcionGlobal: FunctionDeclaration): string {
  let callFunction: string = '';
  if (!funcionGlobal.isExported()) {
    callFunction = `(${aliasModuleExport(sourceFile)} as any).`;
  } else if (funcionGlobal.isExported()) {
    callFunction = `${aliasModuleExport(sourceFile)}.`;
  }
  return callFunction;
}

function generarTestFuncion(
  funcionGlobal: FunctionDeclaration,
  sourceFilesTodo: SourceFile,
  tgcontent: TestGeneratorContext
) {
  let extracionFuncion = '';
  if (funcionGlobal.isExported() || funcionGlobal.isImplementation()) {
    extracionFuncion += `it("Unit Test for ${funcionGlobal.getName()}",${funcionGlobal.isAsync()?'async':''}() =>{`;
    extracionFuncion += `\n\ttry{`;
    const parametrosN = parametrosNombre(funcionGlobal);
    let variablesObjetoParams = objetoParametroMetodo(funcionGlobal, sourceFilesTodo);
    //si no teneemos parametros para este metodo , no sacamos la variable con el objeto y solo sacamaos el componente en null
    if (variablesObjetoParams !== undefined) {
      extracionFuncion += variablesObjetoParams;
      extracionFuncion += `\n\t ${addTextControlAwaitPromise(funcionGlobal)}${functionsInsideService(
        sourceFilesTodo,
        funcionGlobal
      )}${funcionGlobal.getName()}(${parametrosN});`;
    } else {
      extracionFuncion += `\n\t ${addTextControlAwaitPromise(funcionGlobal)}${functionsInsideService(
        sourceFilesTodo,
        funcionGlobal
      )}
      ${funcionGlobal.getName()}();`;
    }
    const spyOnGenerator = new SpyOnGenerator(); 
    extracionFuncion += '\n' + spyOnGenerator.getSpyOnForMethod(funcionGlobal, tgcontent).join('\n');
    //extracionFuncion += `\n\texpect(resultado).toBe(expectedOutput);`;
    extracionFuncion += `\n\t} catch (err){ console.log('Error in test ${funcionGlobal.getName()}', err);}`;
    extracionFuncion += '\n});\n';
    const arrayStatements = funcionGlobal.getStatements();
    //console.log(arrayStatements.map(element=>element.getText()));
    if (mastest(funcionGlobal)) {
      extracionFuncion += mastest(funcionGlobal);
    }
  }
  return extracionFuncion;
}

export function claseOne(sourceFile: SourceFile) {
  const clases = extraerClase(sourceFile);
  for (const clase of clases) {
    return clase;
  }
}

export function escrituraArchivoTest(filename: string, sourceCode: string) {
  if (sourceCode.length === 0) {
    // Este fichero no tiene tests
    return;
  }
  // Initialize a new project
  const project = new Project();

  if (fs.existsSync(filename)) {
    fs.rmSync(filename);
  }
  // Create a source file
  const sourceFile = project.createSourceFile(filename, sourceCode);

  // Format the code (this applies formatting based on project settings)
  sourceFile.organizeImports();
  sourceFile.fixMissingImports();
  sourceFile.fixUnusedIdentifiers();
  sourceFile.formatText({
    semicolons: ts.SemicolonPreference.Insert,
  });

  // Save the file to the file system
  project.save();
  console.log(`File created and formatted at: ${filename}`);
}

//si coincide un string que le pasamos al uno de los metodos de ese archivo, sacamos de este el test
export function testSelection(sourceFilesTodo: SourceFile, busquedaString: string, tgcontent: TestGeneratorContext) {
  console.log('por aqui empiezo');
  const clase = claseOne(sourceFilesTodo);
  let testMetodo = '';
  const nombreMetodo = busquedaString.match(/(\w+)\(/)?.[0].replace('(', '');
  if (clase && nombreMetodo) {
    const method = clase.getMethods().find(m => m.getName() === nombreMetodo);
    if (method) {
      testMetodo = procesarMetodo(method, clase, sourceFilesTodo, tgcontent).getTemplate();
    }
    if (findDecoratorclase(clase,'injectable')) {
      testMetodo = testMetodo.replace(/component/g, 'service').replace(/let\s+fixture(.+)/g, '');
    }
  }
  return testMetodo;
}

function dependencyInject(sourceFile: SourceFile): string[] {
  let result: string[] = [];
  const linesWithInject: string[] | undefined = sourceFile
    ?.getText()
    .split('\n')
    .filter(line => line.includes('inject('));
  if (linesWithInject) {
    linesWithInject.map(line => {
      const match = line.match(/(?<= inject\().+?\w+/g);
      if (match) {
        const elementInject: string = match[0];
        result.push(elementInject);
      }
    });
  }
  return result;
}

export function initializesDependecyOrService(sourceFile: SourceFile): string {
  let result = '';
  const arrayDependecyInject: string[] = dependencyInject(sourceFile);
  for (const dependencyName of arrayDependecyInject) {
    result += `\n\t\tlet ${convertirMayusEnMinus(dependencyName)} : ${dependencyName}`;
  }
  return result;
}

export function assignmentValueDependencyInject(sourceFile: SourceFile): string {
  let result: string = '';
  const arrayDependecyInject: string[] = dependencyInject(sourceFile);
  for (const dependencyInject of arrayDependecyInject) {
    result += `\n\t\t${convertirMayusEnMinus(dependencyInject)} = TestBed.inject(${dependencyInject})`;
  }
  return result;
}

export function findDecoratorclase(clase: ClassDeclaration,nameDecorator:string): boolean {
  let decoratorTrue = false;
  const decoradorService = clase.getDecorators().find(decorator => decorator.getName() === nameDecorator);
  if (decoradorService) {
    decoratorTrue = true;
  }
  return decoratorTrue;
};


export function generateTest(input: string, output: string, tgcontent: TestGeneratorContext) {
  //console.log('tgcontent.tsconfigPath', tgcontent.tsconfigPath);
  const sourceProject: SourceProject = tgcontent.sourceProject;
  //objeto de tipo sourceFile para despues sacar otros objetos de este
  const sourceFilesTodo = sourceProject.findFile(input);

  if (!sourceFilesTodo) {
    return;
  }

  let clases = extraerClase(sourceFilesTodo);
  let funcionesGlobales = extraerFuncionesGlobales(sourceFilesTodo);
  if (funcionesGlobales.length !== 0) {
    let testFuncion = '';
    testFuncion += importFunction(sourceFilesTodo, funcionesGlobales);
    const rutaFuncionesG = input.replace(/(.+)\\|\.ts/gm, '');
    testFuncion += `describe('Test ${rutaFuncionesG}', () => {`;
    for (const funcionGlobal of funcionesGlobales) {
      testFuncion += generarTestFuncion(funcionGlobal, sourceFilesTodo, tgcontent);
    }
    testFuncion += '});';
    escrituraArchivoTest(output, testFuncion);
  } else {
    
    for (const clase of clases) {
      const nombreClase = clase.getName() ?? '';
      const variableName = nombreClase.charAt(0).toLowerCase() + nombreClase.slice(1);
      const mocksElementImport = mocksElementoImportados(clase, input, sourceFilesTodo, sourceProject) ?? '';
      procesarClase(clase);
      const templateDescribe:TeplateDescribe = new TeplateDescribe();
      const metodos = extraerMetodosClase(clase);

      if (input.match(/(.+)?module.ts/gm)) {
        console.log(input);
        console.log('No añadimos tests a un fichero de modulo');
      } else if (input.match(/(.+)?component.ts/gm) || clases.length !== 0) {
        templateDescribe.tests.push(itShouldCreate(variableName));
        templateDescribe.tests.push(...propertiesCalculated(clase));
        for (const metodo of metodos) {
          templateDescribe.tests.push(procesarMetodo(metodo, clase, sourceFilesTodo, tgcontent));
          
          // TODO @mbaquerr investigar la parte de tests adicionales
          /*
          if (mastest(metodo)) {
            testMetodo += mastest(metodo);
          }
          if (otroTestElementArray(metodo)) {
            testMetodo += otroTestElementArray(metodo);
          }
          */
        }
        // TODO @mbaquerr investigar tests para static
        /*
        if (constantsExportedSyntaxFunction(sourceFilesTodo)) {
          testMetodo += constantsExportedSyntaxFunction(sourceFilesTodo);
        }
        */
        

        //leemos el fichero ts wrapperClase
        //aqui poner los import que sean comunes
        let importCommon = `import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';`;

        const reconstruccionObject = construccionObjeto(sourceFilesTodo?.getText());
        let mocks = '';

        mocks += safeEmptyStr(mocksElementImport, '\n');
        mocks += safeEmptyStr(mocksGlobales(sourceFilesTodo?.getText(), clase), '\n');
        mocks += safeEmptyStr(reconstruccionObject, '\n');
        mocks += safeEmptyStr(windowsConstruccionObjecto(sourceFilesTodo?.getText(), reconstruccionObject), '\n');

        const rutaComponente = input;
        const palabras = rutaComponente.split('\\');
        const ultimaPalabra = palabras[palabras.length - 1].replace(/\.ts/, '');
        let importsExtra = sacarImports(sourceFilesTodo);
        importsExtra += modulosImportados(clase, 'imports');
        importsExtra += modulosImportados(clase, 'declarations');
        importsExtra += '\nimport { {{testing-file}} } from ' + "'./" + ultimaPalabra + "';";
        for (const importEnum of añadirImportEnum) {
          importsExtra += `\n${importEnum}`;
        }
        if (dependencyLibraryTesting() === true) {
          importsExtra += `\nimport { MockModule, MockComponent } from 'ng-mocks';`;
        }
        importsExtra += `\n\n${importTypeAlias(sourceFilesTodo)}`;
        //const importsIdentifiers = sacarElementsImportsExtra(
        //sacarImports(sourceFilesTodo)
        //);
        
        let declarationcomponent = '';
        
        let componentFixture = '';
        componentFixture += `${initializesDependecyOrService(sourceFilesTodo)}\n`;
        const isComponent = findDecoratorclase(clase,'Component');
        const isService = findDecoratorclase(clase,'Injectable');
        const templateTestBed = new TemplateTestbed();
        if (findDecoratorclase(clase,'Component') || findDecoratorclase(clase,"Injectable")) {
          componentFixture += `let ${variableName}: {{testing-file}};\nlet fixture: ComponentFixture<{{testing-file}}>;\n`;
          importCommon += `\nimport { ComponentFixture, TestBed } from '@angular/core/testing';\n\n`;
        }

        if (isService) {
          templateTestBed.injects = `${variableName} = TestBed.inject({{testing-file}});`;
          
        } else if (isComponent) {
          //aqui poner la declaracion del component
          templateTestBed.declarations.push(...anadirModulos(clase, 'declarations'));
          templateTestBed.imports.push(...anadirModulos(clase, 'imports'));
          //templateTestBed.imports.push(...modulosExtras(sourceFilesTodo)); // TODO @mbaquerr investigar como añadir reduxunittestmodule
          templateTestBed.providers.push(`${elementProviders(clase)}`);
          templateTestBed.injects = `${assignmentValueDependencyInject(sourceFilesTodo)}`;
          templateTestBed.source = `fixture = TestBed.createComponent({{testing-file}});
            ${variableName} = fixture.componentInstance;`;
          if (mocksElementImport) {
            templateTestBed.source += `
            ${variableName}.${namedElementInput(clase)} = mock${namedVariable(clase, input, sourceFilesTodo, sourceProject)};`;
          }
        }
        // leemos el fichero y sacamos los component-declaration y el de import y el de test
        //de este fichero elimino los elementos que no necesito que serian los comentarios que hemos sacado antes por otro string que se lo pasamos
        //el string se puede cambiar mas arriba por los import que hay comunes y la declaracion del componente que sea
        
        templateDescribe.imports = importCommon + '\n' + importsExtra;
        templateDescribe.className = nombreClase;
        templateDescribe.commonMocks = '\n//mocks\n' + mocks + '\n//componentFixture\n' + componentFixture + '\n//declarationcomponent\n' + declarationcomponent;
        const templateBeforeEach = new TemplateBeforeEach();
        templateBeforeEach.source = templateTestBed.getTemplate();
        templateBeforeEach.isAsinc = templateTestBed.isAsinc;
        templateDescribe.beforeEach.push(templateBeforeEach);
        
        //console.log("probando");

        escrituraArchivoTest(output, templateDescribe.getTemplate());
      }
      
    }
  }
}

export function generateTestForSelection(
  fileName: string,
  selected: string,
  output: string,
  tgcontent: TestGeneratorContext
) {
  const sourceProject: Project = new Project({ tsConfigFilePath: tgcontent.tsconfigPath.replace('/c:', '') });
  sourceProject.addSourceFilesAtPaths(fileName);
  const sourceFilesTodo = sourceProject.getSourceFile(fileName);
  if (!sourceFilesTodo) {
    return;
  }
  const testMetodo = testSelection(sourceFilesTodo, selected, tgcontent);
  const testCompleto = fs.readFileSync(output, 'utf8');
  //si no esta includo ese metodo o hay ya una variacion en el archivo, no lo remplazamos si no que lo sacamos al final del mismo
  const isMatch = testCompleto.includes(testMetodo);
  //si ya se encuentra entonces le decimos que lo remplace en la cadena
  if (isMatch) {
    const textoRemplazado = testCompleto.replace(testMetodo, testMetodo);
    escrituraArchivoTest(output, textoRemplazado);
  } else {
    if (testCompleto) {
      fs.appendFile(output, testMetodo, function (err: any) {
        if (err) {
          console.error(err);
        } else {
          console.log('El archivo se guardo correctamente.');
        }
      });
    } else {
      console.log(`No se encuentra${output}->Lanza primero un test de todo el archivo`);
    }
  }
}
