export const lista_array = [
  'concat',
  'every',
  'filter',
  'find',
  'findIndex',
  'forEach',
  'includes',
  'indexOf',
  'join',
  'lastIndexOf',
  'map',
  'pop',
  'push',
  'reduce',
  'reverse',
  'shift',
  'slice',
  'some',
  'sort',
  'splice',
  'unshift',
];

export const listaBlancaSpyOn = [
  // cambiamos esta lista para la que necesitaemos
  '.push',
  '.pop',
  '.shift',
  '.unshift',
  '.splice',
  '.slice',
  '.concat',
  '.join',
  '.reduce',
  '.map',
  '.filter',
  '.forEach',
  '.some',
  '.every',
  '.find',
  '.findIndex',
  '.includes',
  '.indexOf',
  '.lastIndexOf',
  '.sort',
  '.reverse',
  // Métodos de String
  '.length',
  '.charAt',
  '.charCodeAt',
  '.indexOf',
  '.lastIndexOf',
  '.slice',
  '.substring',
  '.substr',
  '.trim',
  '.trimStart',
  '.trimEnd',

  // Métodos de modificación
  '.toUpperCase',
  '.toLowerCase',
  '.toLocaleUpperCase',
  '.toLocaleLowerCase',
  '.replace',
  '.search',
  '.match',
  '.split',
  '.concat',
  '.join',
  '.repeat',
  '.padStart',
  '.padEnd',

  // Métodos de validación
  '.endsWith',
  '.startsWith',
  '.includes',
  '.matchAll',
  '.searchAll',
  '.splitAll',

  // Métodos de conversión
  '.valueOf',
  '.toString',
  '.toNumber',
  '.toBoolean',
  '.toDate',
  '.toTime',
  '.toDateString',
  '.toTimeString',
  '.toLocaleDateString',
  '.toLocaleTimeString',

  // Métodos de formato
  '.padStart',
  '.padEnd',
  '.repeat',
  '.format',

  //metodos de Promesas
  'Promise',
  'new Promise',
  '.then',
  '.catch',
  '.finally',
  '.executor',
  '.resolve',
  '.reject',
  '.pending',
  '.fulfilled',
  'rejected',
  'async',
  'await',
];
export const listaBlancaparametros = [
  // cambiamos esta lista para la que necesitaemos
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'slice',
  'concat',
  'join',
  'reduce',
  'map',
  'filter',
  'forEach',
  'some',
  'every',
  'find',
  'findIndex',
  'includes',
  'indexOf',
  'lastIndexOf',
  'sort',
  'reverse',
  // Métodos de String
  'length',
  'charAt',
  'charCodeAt',
  'indexOf',
  'lastIndexOf',
  'slice',
  'substring',
  'substr',
  'trim',
  'trimStart',
  'trimEnd',

  // Métodos de modificación
  'toUpperCase',
  'toLowerCase',
  'toLocaleUpperCase',
  'toLocaleLowerCase',
  'replace',
  'search',
  'match',
  'split',
  'concat',
  'join',
  'repeat',
  'padStart',
  'padEnd',

  // Métodos de validación
  'endsWith',
  'startsWith',
  'includes',
  'matchAll',
  'searchAll',
  'splitAll',

  // Métodos de conversión
  'valueOf',
  'toString',
  'toNumber',
  'toBoolean',
  'toDate',
  'toTime',
  'toDateString',
  'toTimeString',
  'toLocaleDateString',
  'toLocaleTimeString',

  // Métodos de formato
  'padStart',
  'padEnd',
  'repeat',
  'format',
];

export const listaTypado = [
  //'Function',
  'Date',
];
export const ComprobarSiEnum: boolean = false;
export let añadirImportEnum = new Set();

export interface TestSyntaxItem {
  spyOn: string;
  mockReturnValue: string;
  mockResolvedValue: string;
  mockTimeouts: string;
  runTimeouts: string;
}

export const spySyntaxItems: {
  [key: string]: TestSyntaxItem
} = {
  jasmine: {
    spyOn: 'spyOn',
    mockReturnValue: 'and.returnValue',
    mockResolvedValue: 'and.callFake',
    mockTimeouts: 'jasmine.clock().install()',
    runTimeouts: 'jasmine.clock().tick(10000);jasmine.clock().uninstall()'
  },
  jest: {
    spyOn: 'jest.spyOn',
    mockReturnValue: 'mockReturnValue',
    mockResolvedValue: 'mockResolvedValue',
    mockTimeouts: 'jest.useFakeTimers()',
    runTimeouts: 'jest.runAllTimers()'
  },
};
