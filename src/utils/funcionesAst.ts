import path from 'path';
import {
  ClassDeclaration,
  FunctionDeclaration,
  MethodDeclaration,
  ModifierableNode,
  Node,
  ParameterDeclaration,
  Project,
  PropertyDeclaration,
  ReferencedSymbol,
  SourceFile,
  Type,
  TypeAliasDeclaration,
  VariableDeclaration,
  ts,
} from 'ts-morph';
import * as vscode from 'vscode';
import { SourceProject } from '../model/SourceProject';
import { TemplateIt } from '../templates/TeplateIt';
import { convertirMayusEnMinus } from './funcionesMocks';
import { typadoAst } from './funcionesParametrosDeMetodos';
import { generateMethodCall } from './funcionesSpyOn';
import { aliasModuleExport } from './testGenerator';
const fs = require('fs');


export function getVariableName(clase: ClassDeclaration | undefined) {
  const className = clase?.getName() ?? '';
  if (clase && className) {
    if (clase.isAbstract()) {
      return clase.getName() || '';
    }
    return className.charAt(0).toLowerCase() + className.slice(1);
  }
  return '';
}

export function extractBalancedCode(code: string, leftChar: string, rightChar: string, fromIndex = 0) {
  let contBraces = 0; //contador iniciado a cero

  let functionCurrIndex = fromIndex;

  let firstCharIndex = 0;

  let codeUntilFirstChar = '';

  // Search for the beggining of the function
  // se crecorre el from index y va aumentando hasta terminar de recorrer toda la cadena de string
  for (let i = fromIndex, max = code.length; i < max; ++i) {
    functionCurrIndex = i; // si el charat del code de ese indice que se va recorriendo es igual a { esto que seria el primer leftchart
    // aumentamos el contador de countbraces y nos salimos el bucle

    if (code.charAt(i) === leftChar) {
      firstCharIndex = functionCurrIndex;

      codeUntilFirstChar = code.substring(fromIndex, firstCharIndex);

      ++contBraces;

      break;
    }
  }

  ++functionCurrIndex;

  // Search for the end of the function

  for (let i = functionCurrIndex, max = code.length; i < max; ++i) {
    functionCurrIndex = i;

    if (contBraces === 0) {
      break;
    }

    if (code.charAt(i) === leftChar) {
      ++contBraces;
    } else if (code.charAt(i) === rightChar) {
      --contBraces;
    }
  }

  return {
    fromIndex: fromIndex,

    toIndex: functionCurrIndex,

    code: code.slice(fromIndex, functionCurrIndex).trim(),

    firstCharIndex,

    codeUntilFirstChar: codeUntilFirstChar.trim(),
  };
}
export function extraerVariables(texto: SourceFile | undefined): VariableDeclaration[] {
  return texto?.getVariableDeclarations() ?? [];
}

//funcion para extraer todos los valores de una variable
export function extraerValorVariables(variables: VariableDeclaration[]) {
  let valorVariable = 'valores: ';
  for (const variable of variables) {
    const initializer = variable.getInitializer();
    if (initializer) {
      valorVariable += initializer.getText() + ',';
    }
  }
  return valorVariable;
}
export function extraerNameVariables(variables: VariableDeclaration[]) {
  let nombreVariable = 'Name Variables: ';
  const arrayNombre = variables.map(variable => variable.getName()).join(',');
  return nombreVariable + arrayNombre;
}

export function extraerFunciones(sourceFile: SourceFile | undefined): FunctionDeclaration[] {
  return sourceFile?.getFunctions() ?? [];
}

export function extraerClase(sourceFile: SourceFile | undefined): ClassDeclaration[] {
  return sourceFile?.getClasses() ?? [];
}

export function paramConstructorClase(clase: ClassDeclaration): ParameterDeclaration[] {
  return clase.getConstructors()[0]?.getParameters();
}

export function extraerFuncionesGlobales(sourceFile: SourceFile | undefined): FunctionDeclaration[] {
  return sourceFile?.getFunctions() ?? [];
}

export function extraerMetodosClase(clase: ClassDeclaration): MethodDeclaration[] {
  return clase.getMethods() || [];
}

export function extraerParametrosClase(params: ClassDeclaration) {
  const parametros = params.getTypeParameters().map(parametro => parametro);
  return parametros;
}

export function extraerPopiedadesClase(clase: ClassDeclaration) {
  const propiedadesClase = clase.getProperties();
  let resultado: {
    tipo: string;
    cname: string;
    visibilidad: string;
  }[] = [];
  for (const propiedad of propiedadesClase) {
    const accesibilidad = propiedad.getText().match(/(private|public|protected)\s/);
    resultado.push({
      tipo: propiedad.getType().getText(),
      cname: propiedad.getName(),
      visibilidad: accesibilidad?.[1] || 'public',
    });
  }
  return resultado;
}

export function extraerPopiedadesClaseTypo(clase: ClassDeclaration) {
  const propiedadesCTipo = clase.getProperties().map(propiedad => propiedad.getType().getText());
  return propiedadesCTipo;
}

export function extraerPopiedadesClaseName(clase: ClassDeclaration) {
  const propiedadesCName = clase.getProperties().map(propiedad => propiedad.getName());
  return propiedadesCName;
}

export function extraerPopiedadesClaseAcces(clase: ClassDeclaration) {
  let propiedadCTexto: string[] = [];
  propiedadCTexto = clase.getProperties().map(propiedad => propiedad.getText());
  let i = 0;
  let resultado = [];
  for (const tipo of propiedadCTexto) {
    const accesibilidad = tipo.match(/(private|public|protected)\s/);
    if (accesibilidad) {
      resultado.push(accesibilidad[1]);
    } else {
      //si no hay nada escrito por defecto lo ponemos como publico
      const accesibilidad = 'public';
      resultado.push(accesibilidad);
    }
    i++;
  }
  return resultado;
}



export function parametrosNombre(metodo: MethodDeclaration | FunctionDeclaration): string[] {
  const parametrosNombre = metodo.getParameters().map(parametro => {
    if (parametro.isRestParameter()) {
      return `...${convertirMayusEnMinus(parametro.getName())}`;
    } else {
      return convertirMayusEnMinus(parametro.getName());
    }
  });
  return parametrosNombre;
}

export function sacarImports(objetoComponent: SourceFile) {
  const textoContent = objetoComponent.getText();
  const imports = textoContent.match(/import\s+(.+?)\s*;/gs);
  let cadenaImportsExtra = '';
  if (imports) {
    for (const importExtra of imports) {
      cadenaImportsExtra += importExtra + '\n';
    }
  }
  return cadenaImportsExtra;
}

export function importFunction(sourceFile: SourceFile, arrayFunciones: FunctionDeclaration[]) {
  const ruta = sourceFile.getBaseName().replace(/\.ts/g, '');
  let importFunciones = '';
  importFunciones += sacarImports(sourceFile);
  if (aliasModuleExport(sourceFile)) {
    importFunciones += `import * as ${aliasModuleExport(sourceFile)} from './${ruta}';\n`;
  }
  importFunciones += 'import {';
  for (const nameFuncion of arrayFunciones) {
    if (nameFuncion.isExported()) {
      importFunciones += `${nameFuncion.getName()}, `;
    }
  }
  importFunciones += `} from './${ruta}';\n`;
  return importFunciones;
}

//guardamos las variables de los @input
export function elementosInput(arrayallInput: RegExpMatchArray[]) {
  const elementInputs: string[] = [];
  if (arrayallInput) {
    for (const array of arrayallInput) {
      elementInputs.push(array[array.length - 2]);
    }
  }
  return elementInputs;
}

function typeAlias(sourceFile: SourceFile): TypeAliasDeclaration[] {
  const typeAliasGlobal = sourceFile.getTypeAliases();
  return typeAliasGlobal;
}
export function nombreAlias(sourceFile: SourceFile): string[] {
  const nombreAlias = typeAlias(sourceFile).map(element => element.getName());
  return nombreAlias;
}

export function typadoAlias(sourceFile: SourceFile): Type<ts.Type>[] {
  const typado = typeAlias(sourceFile).map(element => element.getType());
  return typado;
}

export function importTypeAlias(sourceFile: SourceFile): string {
  let resultado = '';
  const arrTypeAlias = typeAlias(sourceFile);
  if (arrTypeAlias) {
    resultado = arrTypeAlias.map(typadoAlias => typadoAlias.getText()).join('\n');
  }
  return resultado;
}

//guardamos los typos
export function typosInput(arrayallInput: RegExpMatchArray[]) {
  const typosPropiedad: string[] = [];
  if (arrayallInput) {
    for (const array of arrayallInput) {
      typosPropiedad.push(array[array.length - 1]);
    }
  }
  return typosPropiedad;
}

//funcion usamos el vscode para sacar la ruta del proyecto que estamos ejecutando los test automaticos
export function rutaProyecto() {
  const workspaceFolders = vscode.workspace.workspaceFolders;

  if (workspaceFolders && workspaceFolders.length > 0) {
    return workspaceFolders[0].uri.fsPath;
  } else {
    //si no lo encuentra ningun proyecto abierto devuelve un erro por consola
    console.log('No se ha encontrado ningún proyecto abierto');
    return undefined;
  }
}
//esto esta listo y funcional
export function sourceFileRuta(ruta: string) {
  // @mbaquerr sustituir por findFile en proyecto
  const projectImport: Project = new Project();
  projectImport.addSourceFileAtPath(ruta);
  return projectImport.getSourceFile(ruta);
}

export function autoImportsVs(moduleName: string) {
  if (!vscode.window.activeTextEditor?.document) {
    return;
  }
  const projectPath =
    vscode.workspace.getWorkspaceFolder(vscode.window.activeTextEditor.document.uri)?.uri.fsPath ?? '';
  // Obtiene la ruta del módulo
  const modulePath = path.join(projectPath, 'node_modules', moduleName);
  //console.log(modulePath);

  // Si el módulo no existe, se muestra un mensaje de error
  if (!fs.existsSync(modulePath)) {
    vscode.window.showErrorMessage(`El módulo "${moduleName}" no existe.`);
    return;
  }

  return modulePath;
}

export function dependencyLibraryTesting(): boolean {
  let bibliotecaPruebas: boolean = false;
  const ruta = `${rutaProyecto()?.replace(/src.+/gm, '')}/package.json`;
  const sourceFile = sourceFileRuta(ruta);
  const packageJson = JSON.parse(sourceFile?.getText()!);
  const dependecias = packageJson.devDependencies;
  for (const dependencyName in dependecias) {
    if (dependencyName.includes('ng-mocks')) {
      bibliotecaPruebas = true;
      break;
    }
  }
  return bibliotecaPruebas;
}

export function anadirModulos(clase: ClassDeclaration, cadena: string) {
  let tipoMock = '';
  const expreReg = new RegExp(`${cadena}:\\s+\\[(.+?)\\]`, 'gs');
  const remplazo = /(\w+):\s+\[|\]/gs;
  let importsModules = modulos(clase, expreReg, remplazo);
  if (cadena === 'declarations') {
    tipoMock = 'MockComponent';
    importsModules = importsModules?.replace(`${clase.getName()},`, '');
  } else {
    importsModules = importsModules?.replace(`CommonModule,`, '');
    tipoMock = 'MockModule';
  }
  const arrayModulos = importsModules?.match(/(\w+)/g);
  let resultado: string[] = [];
  if (dependencyLibraryTesting() === true && arrayModulos) {
    for (const module of arrayModulos) {
      resultado.push(`${tipoMock}(${module})`);
    }
  } else if (importsModules) {
    resultado.push(importsModules);
  }
  return resultado;
}

export function modulosImportados(clase: ClassDeclaration, cadena: string) {
  let importsModule = '';
  const sourceFile = rutaModulosClase(clase);
  const remplace = /(\w+):\s+\[|\]/gs;
  const expreRegComponent = new RegExp(`${cadena}:\\s+\\[(.+?)\\]`, 'gs');
  if (sourceFile) {
    const arrayImports = sourceFile.getImportDeclarations().map(element => element.getText());
    const nombresModulos = modulos(clase, expreRegComponent, remplace)
      ?.replace(/\r\n\s+/gm, '')
      .split(',');
    const arrayEImports = [];
    for (const nombreM of nombresModulos || []) {
      const arraryelementosEncontrados = arrayImports.filter(element => element.includes(nombreM));
      arrayEImports.push(arraryelementosEncontrados[0]);
    }
    const sinDuplicados = [...new Set(arrayEImports.map(element => element))];
    for (const importado of sinDuplicados) {
      if (cadena === 'declarations' && clase.getName()) {
        if (!importado.includes(clase.getName()!)) {
          importsModule += '\n' + importado.replace(/\.\//gm, '../../');
        }
      } else {
        importsModule += '\n' + importado;
      }
    }
  } else {
    importsModule = `import { CommonModule } from '@angular/common'`;
  }
  return importsModule;
}

function rutaModulosClase(clase: ClassDeclaration):SourceFile | "" | undefined {
  const referenciasClases:ReferencedSymbol[] = clase.findReferences();
  if (referenciasClases?.[1]) {
    let ruta: string = '/' + referenciasClases[1].compilerObject.definition.fileName;
    ruta = ruta.replaceAll('//', '/');
    const sourceFile:SourceFile | undefined = sourceFileRuta(ruta);
    return sourceFile;
  }
  return '';
}

//funcion por ahora de prueba para buscar si en un ruta que le paso a mano por ahora , encuentra el nombre de la clase
export function modulos(clase: ClassDeclaration, expreReg: RegExp, remplazo: RegExp) {
  const sourceFile = rutaModulosClase(clase);
  if (sourceFile) {
    const contenidoNgModule = sourceFile.getText().match(/@NgModule(.+?)\}/gs);
    if (contenidoNgModule) {
      const nombreClase = new RegExp(`${clase.getName()}`).exec(contenidoNgModule[0]);
      if (nombreClase) {
        let importsModule = expreReg.exec(contenidoNgModule[0])?.[0].replace(remplazo, '');
        return importsModule;
      }
    }
  } else {
    return 'CommonModule,\n';
  }
  return '';
}

//sacamos toda la cadena de todos los elementos que haya en una clase que sean @input
export function allInput(clase: ClassDeclaration): RegExpMatchArray[] {
  const typoPropiedad:RegExpMatchArray[] = [];
  const propiedadesTexto:string[] = clase.getProperties().map(propiedad => propiedad.getText());
  for (const textoPropiedad of propiedadesTexto) {
    const matches:RegExpMatchArray | null = textoPropiedad.match(/@Input\(\)(.+);/g);
    if (matches) {
      const words:RegExpMatchArray | null = matches[0].match(/(\w+)/g);
      if (words) {
        typoPropiedad.push(words);
      }
    }
  }
  return typoPropiedad;
}

//sacamos el contenido del componente donde se esta utilizando interfaces o clases.
export function contenidoElementoImportado(
  sourceFile: SourceFile,
  arrayElementosInput: string[],
  input: string,
  sourceProject: SourceProject
) {
  const arrayimportaciones = sourceFile?.getImportDeclarations();
  if (arrayimportaciones) {
    for (const elementoInput of arrayElementosInput) {
      for (const importDeclaration of arrayimportaciones) {
        if (importDeclaration.getText().includes(elementoInput)) {
          const importModule = importDeclaration.getModuleSpecifier();
          let ruta = importModule.getText();
          //mejorar esto de la ruta exportando el extension.ts por mejorar el codigo
          ruta = input.replace(/src(.*)/gs, ruta).replace(/\'/g, '');
          ruta += '.ts';
          const sourceFileImport = sourceProject.findFile(ruta);
          return sourceFileImport?.getText();
        }
      }
    }
  }
}

//todo el texto del elemento que necesitamos para hacer el mocks especifico para el test automatico
export function mocksElementoImportado(texto: string, arrayElementosInput: string[]) {
  for (const elementoInput of arrayElementosInput) {
    let matches: any = new RegExp(`${elementoInput}\\s+\\{(.+)\\}`, 'gs').exec(texto);
    if (matches) {
      matches = matches.map((contenidoInterface: any) => {
        return {
          elbloque: '',
          startIndex: texto.indexOf(contenidoInterface),
        };
      });
      if (matches) {
        matches = matches.map(
          (contenidoInterface: any) =>
            ((contenidoInterface as any).elbloque = extractBalancedCode(
              texto,
              '{',
              '}',
              (contenidoInterface as any).startIndex
            ).code)
        );
        if (matches) {
          return matches[0];
        }
      }
    }
  }
}


//ts.SyntaxKind.ComputedPropertyName
export function propertiesCalculated(clase: ClassDeclaration): TemplateIt[] {
  
  const properties: PropertyDeclaration[]|undefined = clase.getProperties().filter(property => {
    if (property.getDescendantsOfKind(ts.SyntaxKind.ReturnStatement).length!==0) {
      return property;
    }
  });
  const templates = [];
  if(properties){
    for (const property of properties) {
      const propertyName: string = property.getName();
      const templateIt = new TemplateIt();
      templateIt.testName = propertyName;
      templateIt.source = generateMethodCall(clase, property);
      templates.push(templateIt);
    }
  }
  return templates;
}

function parametrosSyntaxFunction(sourceFile:SourceFile):(Node|undefined)[]{
   const params:(Node|undefined)[]=sourceFile.getVariableDeclarations().flatMap(element=>element.getDescendants().find(element=>{
    if(element.getKindName()==='Parameter'){
      return element;
    }}));
      return params;
};

export function constantsExportedSyntaxFunction(sourceFile:SourceFile):string{
  let testConstants:string='';
  const params:(Node|undefined)[]=parametrosSyntaxFunction(sourceFile);
  const constantsSyntaxFunction:VariableDeclaration[]=sourceFile.getVariableDeclarations().filter(variable=>{
    if(variable.getDescendantsOfKind(ts.SyntaxKind.ReturnStatement)&& variable.isExported()){
      return variable;
    }});
    for (const variable of constantsSyntaxFunction) {
      const variableName: string = variable.getName();
      testConstants += `it("test for ${variableName}",  () =>{`;
      testConstants += `\n\ttry{`;
      if(params!==undefined){
      let parametrosString:string='';
      for(const param of params){
        const nameParam:string|undefined=param?.getFirstChild()?.getText();
        const typeParam:Type|undefined=param?.getType();
      testConstants += `\n\t const ${nameParam}:${typeParam?.getText().replace(/import(.+)\.|'/gm, '')} =`;
      testConstants +=`${typadoAst("",typeParam)};`;
      parametrosString +=`${nameParam},`;
      }
      testConstants += `\n\t ${variableName}(${parametrosString.slice(0,-1)});`;
      testConstants += `\n\t} catch (err){ console.warn('Error in test ${variableName}}', err);}`;
      testConstants += '\n});\n\n';
    }else{
      testConstants += `\n\t ${variableName}();`;
      testConstants += `\n\t} catch (err){ console.warn('Error in test ${variableName}}', err);}`;
      testConstants += '\n});\n\n';
    }
    }
    return testConstants;
    
  };
export function elementProviders(clase: ClassDeclaration) {
  let providers:string = '';
  if (paramConstructorClase(clase) !== undefined) {
    providers = paramConstructorClase(clase)
      .reduce((acc, element) => {
        const elementType:string = element
          .getType()
          .getText()
          .replace(/import(.+)\.|'/gm, '');
        return `${acc}${elementType}, \n\t\t`;
      }, '')
      .slice(0, -2);
  }
  return providers;
}

export function modulosExtras(sourceFile: SourceFile):string {
  let añadido:string = '';
  let module:string = '';
  const textoComponent:string = sourceFile.getText();
  const moduloEntornoPrueba:RegExpMatchArray | null = textoComponent.match(/const\s+(\w+)\s+=\s+new\s+(\w+)/gs);
  if (moduloEntornoPrueba) {
    añadido += `\t//necesita de un modulo de entorno de pruebas`;
    module = 'ReduxUnitTestModule';
  }
  return `${añadido}\n ${module}`;
}

export function comprobarListaArray(expresion: any, listaArray: string[]) {
  //le pasamos la lista blanca que sea en el parametro de la listaArray
  let call = expresion;
  let arrayLista = [];
  for (const lista of listaArray) {
    //si coincide parte de la cadena , añadimos todas las cadenas de nuevo al array menos esa , todas las cadenas que el resultado sean -1 que no tengan ninguna coincidencia
    for (let llamadas of call) {
      if (llamadas.indexOf(lista) !== -1) {
        arrayLista.push(llamadas);
      }
    }
  }
  for (let sinarray of arrayLista) {
    call = call.filter((elemento: string | any[]) => {
      return elemento.indexOf(sinarray) === -1;
    });
  }

  return call;
}

export function comprobarListaArrayMetodos(expresion: any, listaArray: string[]) {
  //le pasamos la lista blanca que sea en el parametro de la listaArray
  let call = expresion;
  let arrayLista = [];
  for (const lista of listaArray) {
    //si coincide parte de la cadena , añadimos todas las cadenas de nuevo al array menos esa , todas las cadenas que el resultado sean -1 que no tengan ninguna coincidencia
    for (let llamadas of call) {
      const llamada = llamadas.slice(1);
      if (llamada.indexOf(lista) !== -1) {
        arrayLista.push(llamadas);
      }
    }
  }
  for (let sinarray of arrayLista) {
    call = call.filter((elemento: string | any[]) => {
      return elemento.indexOf(sinarray) === -1;
    });
  }

  return call;
}

export function modifiersAccess(metodoOrParam: ModifierableNode, numerTsSyntaxKind: number): boolean {
  const checkModifier: boolean = metodoOrParam.hasModifier(numerTsSyntaxKind);
  return checkModifier;
}

export function convertirNodeAMethodOrParam(nodo: Node): MethodDeclaration | undefined {
  let methodOrParam: MethodDeclaration;
  if (nodo.getKindName() === 'MethodDeclaration') {
    methodOrParam = nodo as MethodDeclaration;
    return methodOrParam;
  }
}