import { StandardizedFilePath } from '@ts-morph/common';
import { ClassDeclaration, MethodDeclaration } from 'ts-morph';
import { listaBlancaparametros } from './constants';
import { getVariableName } from './funcionesAst';
import { analysisFileJson } from './testGenerator';

export function buscarObjeto(sourceFile: string, clase: ClassDeclaration) {
  const elementosMocksWindows = sourceFile.match(/\((window\s+.+?)(;|=|\()/gs);
  let resultado = '';
  if (elementosMocksWindows) {
    let arraymockWindows: string | string[] = '';
    for (const oneMocks of elementosMocksWindows) {
      let mockWindows = oneMocks.replace(/\n\s+|;|=|\(/g, '');
      arraymockWindows = mockWindows.split('.').slice(-1);
    }
    const propiedadesCTipo = clase.getProperties().map(propiedad => {
      if (propiedad.getName() === arraymockWindows[0]) {
        return propiedad.getType().getText();
      }
    });
    const propiedadCTipo = propiedadesCTipo.filter(element => element !== undefined);

    if (propiedadCTipo[0] === 'string') {
      resultado = `'${arraymockWindows}'`;
    } else {
      resultado = `(() => {})`;
    }
    return resultado;
  }
}
export function referenciaObject(sourceFile: string) {
  const elementosMocksWindows = sourceFile.match(/\((window\s+.+?)(;|=|\()/gs);
  if (elementosMocksWindows) {
    let arraymockWindows;
    for (const oneMocks of elementosMocksWindows) {
      let mockWindows = oneMocks.replace(/\n\s+|;|=|\(/g, '');
      arraymockWindows = mockWindows.split('.').slice(-1);
    }
    return arraymockWindows;
  }
}
export function isTypeNumber(propiedad: string, texto: string) {
  let number = '';
  const isNumber = new RegExp(`${propiedad}(:|\\s+:)\\s+[0-9]`, 'gs').exec(texto);
  if (isNumber) {
    number = ` 1;`;
  }
  return number;
}
export function isTypeBoolean(propiedad: string, bodyMetodo: string) {
  let boleano = '';
  const referenciaPropiedad = new RegExp(`${propiedad}\\s+=\\s+(\\w+)`, 'gs').exec(bodyMetodo);
  if (referenciaPropiedad) {
    const propiedadRef = referenciaPropiedad[0].replace(/(\w+)\s+=\s+/gs, '');
    const otroBoolean = new RegExp(`${propiedadRef}\\s+=\\s+(true|false)`, 'gs').exec(bodyMetodo);
    if (otroBoolean) {
      boleano = ` true;`;
    }
  }
  const isBoolean = new RegExp(`${propiedad}\\s+=\\s+(true|false)`, 'gs').exec(bodyMetodo);
  if (isBoolean) {
    boleano = ` true;`;
  }
  return boleano;
}

export function isTypeArray(propiedad: string, bodyMetodo: string) {
  let array = '';
  const objetoPropiedad = propiedad.slice(1);
  const objetoArray = new RegExp(`${objetoPropiedad}(\\[\\]|\\s+\\[\\])`, 'gs').exec(bodyMetodo);
  const isArray = new RegExp(`${propiedad}\\.(length|push)`, 'gs').exec(bodyMetodo);
  if (isArray) {
    array = `[];`;
  } else if (objetoArray) {
    array = `[];`;
  }
  return array;
}
export function isTypeObject(propiedad: string, bodyMetodo: string) {
  let objeto = '';
  const objetoPropiedad = propiedad.slice(1);
  const objetoObject = new RegExp(`${objetoPropiedad}(\\s+\\{\\}|\\{\\})`, 'gs').exec(bodyMetodo);
  const isObject = new RegExp(`${propiedad}\\s+=\\s+\\{`, 'gs').exec(bodyMetodo);
  if (isObject) {
    objeto = `{};`;
  } else if (objetoObject) {
    objeto = `{};`;
  }
  return objeto;
}
export function isFindVariable(propiedad: string, bodyMetodo: string) {
  let variableDeclaration = '';
  const matchFind = new RegExp(`const\\s+(\\w+)(.+)?${propiedad}\\(`, 'gs').exec(bodyMetodo);
  if (matchFind) {
    const variable = matchFind[0].replace(/(const\s+)|=(.+)|:(.+)/gs, '').replace(/\s+/g, '');
    variableDeclaration += `const ${variable}: any = {};`;
  }
  return variableDeclaration;
}

export function isPrivate(method: MethodDeclaration) {
  const textoMetodo = method.getText();
  const metodoPri = textoMetodo.match(/(private|public|protected)\s/g);
  if (metodoPri) {
    if (metodoPri[0] === 'private ') {
      return true;
    }
  }
  return false;
}

export function isFindPropiedad(propiedad: string, bodyMetodo: string) {
  let findAndUso = '';
  let variable = isFindVariable(propiedad, bodyMetodo);
  if (variable) {
    variable = variable.replace(/const\s+|:\s+any\s+=\s+{};/gs, '').replace(/\s+/g, '');
    const propiedadesVariables = new RegExp(`${variable}\\.(.*)?(\\)|;|\\s+)`, 'g').exec(bodyMetodo);
    if (propiedadesVariables) {
      const cadena = propiedadesVariables[0].replace(/=(.*)/gs, '');
      const valorPropiedad = propiedadesVariables[0].replace(/(.*)?(=)/gs, '').replace(/\);/g, '');
      const cadenaDefragmentada = cadena.split('.');
      let refactorizar = cadenaDefragmentada[0];
      for (let i = 1; i < cadenaDefragmentada.length; i++) {
        if (i === cadenaDefragmentada.length - 1) {
          refactorizar = refactorizar + `.${cadenaDefragmentada[i]}`;
          findAndUso += `\t${refactorizar} = ${valorPropiedad.replace(/\s+/g, '')};\n`;
        } else {
          refactorizar = refactorizar + `.${cadenaDefragmentada[i]}`;
          findAndUso += `\t${refactorizar} = {};\n`;
        }
      }
    }
  }
  return findAndUso;
}

export function llamadaMethodMocksComponentPropiedad(metodo: MethodDeclaration, llamadaMetodo: string) {
  let nombreMethod = '';
  const matchesLlamadasMetodos = new RegExp(`this.${llamadaMetodo}\\(`, 'gs').exec(metodo.getText());
  if (matchesLlamadasMetodos) {
    nombreMethod += metodo.getName() + ',';
  }
  let arrayNombreMethod = nombreMethod.split(',');
  arrayNombreMethod = arrayNombreMethod.slice(0, -1);
  return arrayNombreMethod;
}

export function mocksComponentPropiedadPrueba(sourceFile: string, propiedad: string) {
  let resultado = '';
  //console.log(propiedad);
  const busqueda = new RegExp(`(const|public)(.+)${propiedad}:\\s+(.+?)\\s`, 'gs').exec(sourceFile);
  if (busqueda) {
    const busquedaLimpia = busqueda[0].replace(new RegExp(`(.*?)${propiedad}:`, 'gs'), '');
    const typado = busquedaLimpia.match(/\w+/gs);
    if (typado) {
      if (typado[0] === 'string') {
        resultado = `'${propiedad}'`;
      } else if (typado[0] === 'Array') {
        resultado = `[]`;
      } else if (typado[0] === 'false' || typado[0] === 'true') {
        resultado = typado[0];
      } else {
        resultado = `{} as any`;
      }
    }
  }
  // console.log(resultado);
  return resultado;
}


export function mocksComponentPropiedad(
  clase: ClassDeclaration,
  arrayPropiedades: string[],
  textoMetodo: string,
  texto: string,
  method: MethodDeclaration
) {
  for (const propiedad of arrayPropiedades) {
    //const variableDeclaration = sourceFile.getVariableDeclaration("rate");
    let resultado = '';
    let matchespropiedad = new RegExp(`(this.${propiedad}\\.(\\w+))(\\s+|;|,|\\.(\\w+))`, 'gs').exec(textoMetodo);
    if (matchespropiedad) {
      const cadena = matchespropiedad[0].replace(/this.|;|,|\s+/g, '');
      const aPropiedades = cadena.split('.');
      let refactorizar = getVariableName(clase);
      //lista blanca con filter e includes.
      const arrayFiltrado = aPropiedades.filter(elemento => {
        return !listaBlancaparametros.includes(elemento);
      });
      // console.log(typadoMockPropiedad(clase,arrayFiltrado,sourceProject));
      for (let i = 0; i < arrayFiltrado.length; i++) {
        if (i === arrayFiltrado.length - 1) {
          refactorizar = refactorizar + `.${arrayFiltrado[i]}`;
          if (isFindVariable(arrayFiltrado[i], textoMetodo)) {
            resultado = resultado.replace(`${arrayFiltrado[i - 1]} || {}`, `${arrayFiltrado[i - 1]} || []`);
            resultado += `\n\t${isFindVariable(arrayFiltrado[i], textoMetodo)}\n`;
            const variable = isFindVariable(arrayFiltrado[i], textoMetodo).replace(/const\s+|:\s+any\s+=\s+{};/gs, '');
            resultado += '\t' + refactorizar.replace(arrayFiltrado[i], 'push(') + variable.replace(/\s+/g, '') + ');\n';
            resultado += `${isFindPropiedad(arrayFiltrado[i], textoMetodo)}\n`;
          } else {
            resultado += `\t${refactorizar} = `;
            if (isTypeBoolean(arrayFiltrado[i], textoMetodo)) {
              resultado += isTypeBoolean(arrayFiltrado[i], textoMetodo) + '\n';
            } else if (isTypeArray(arrayFiltrado[i], textoMetodo)) {
              resultado += isTypeArray(arrayFiltrado[i], textoMetodo) + '\n';
            } else if (isTypeNumber(arrayFiltrado[i], texto)) {
              resultado += isTypeNumber(arrayFiltrado[i], texto) + '\n';
            } else if (isTypeObject(arrayFiltrado[i], textoMetodo)) {
              resultado += isTypeObject(arrayFiltrado[i], textoMetodo) + '\n';
            } else if (mocksComponentPropiedadPrueba(texto, arrayFiltrado[i])) {
              resultado += `${mocksComponentPropiedadPrueba(texto, arrayFiltrado[i])};\n`;
            } else {
              resultado += `'${arrayFiltrado[i]}';\n`;
            }
            //console.log(resultado);
          }
        } else {
          if (isPrivate(method) === true) {
            refactorizar = refactorizar + `['${arrayFiltrado[i]}']`;
            resultado += `\t(${refactorizar} as any) = ${refactorizar} || {};\n`;
          } else {
            refactorizar = refactorizar + `.${arrayFiltrado[i]}`;
            resultado += `\t(${refactorizar} as any) = ${refactorizar} || {};\n`;
          }
        }
      }
      return resultado;
    }
  }
}

export function windowsConstruccionObjecto(sourcefile: string, texto: string) {
  let resultado = '';
  let elementosMocksWindows = sourcefile.match(/\(((window|document)\s+as any\).+?)(;|=|\()/gs);
  if (elementosMocksWindows) {
    const cadena = elementosMocksWindows[0].replace(/\n\s+|;|=|\(/g, '');
    if (texto) {
      const textoMatch = texto.match(/const\s+(\w+)/g);
      const funcion = textoMatch?.map(elemento => elemento.replace(/const\s+/, ''));
      if (funcion && funcion.length > 0) {
        const ultimaConstante = funcion[funcion.length - 1];
        const primeraFuncion = ultimaConstante.replace(/Function/, '');
        resultado = `(${cadena}.${primeraFuncion.replace(/\s/g, '')} = (${cadena}. ${primeraFuncion.replace(
          /\s/g,
          ''
        )} || ${ultimaConstante.replace(/\s/g, '')};\n`;
      }
    }
  }
  return resultado;
}

export function construccionObjeto(sourceFile: string) {
  let recontruccionObjet = '';
  const elemento = referenciaObject(sourceFile);
  let matchesElemento = new RegExp(`(this.${elemento}.+?)(;|=s+)`, 'gs').exec(sourceFile);
  if (matchesElemento) {
    const cadena = matchesElemento[0]
      .replace(/this.(.+)/g, '')
      .replace(/\((.+)\)?/g, '')
      .replace(/\n\s+/gs, '');
    const arrayElementos = cadena.split('.').slice(1);
    for (let i = arrayElementos.length - 1; i >= 0; i--) {
      if (i === arrayElementos.length - 1) {
        recontruccionObjet += `const ${arrayElementos[i].replace(/\s/g, '')}Function = (() => {});\n`;
        recontruccionObjet += `const ${arrayElementos[i].replace(/\s/g, '')}Object = {\n\t${arrayElementos[i].replace(
          /\s/g,
          ''
        )}: ${arrayElementos[i].replace(/\s/g, '')}Function\n}`;
      } else if (i === 0) {
        recontruccionObjet += `const ${arrayElementos[i].replace(/\s/g, '')}Function = (() => ${arrayElementos[
          i + 1
        ].replace(/\s/g, '')}Object);\n`;
      } else {
        recontruccionObjet += `\nconst ${arrayElementos[i].replace(/\s/g, '')}Function = () =>${arrayElementos[
          i + 1
        ].replace(/\s/g, '')}Object;\n`;
        recontruccionObjet += `const ${arrayElementos[i].replace(/\s/g, '')}Object = {\n\t${arrayElementos[i].replace(
          /\s/g,
          ''
        )}: ${arrayElementos[i].replace(/\s/g, '')}Function\n}\n`;
      }
    }
  }
  return recontruccionObjet;
}

export function mocksGlobales(sourceFile: string, clase: ClassDeclaration) {
  const elementosMocksWindows = sourceFile.match(/\(((window|document)\s+as any\).+?)(;|=|\()/gs);
  let cadenaMock = '';
  if (elementosMocksWindows) {
    let arraymockWindows;
    for (const oneMocks of elementosMocksWindows) {
      let mockWindows = oneMocks.replace(/\n\s+|;|=|\(/g, '');
      arraymockWindows = mockWindows.split('.');
    }
    let i = 0;
    if (arraymockWindows) {
      let elemento = arraymockWindows[0];
      while (arraymockWindows[i] !== undefined) {
        if (arraymockWindows[i + 2] === undefined) {
          elemento = elemento + '.' + arraymockWindows[i + 1];
          cadenaMock += '(' + elemento + ' = (' + elemento + ' || ' + buscarObjeto(sourceFile, clase) + ';\n';
          break;
        } else {
          elemento = elemento + '.' + arraymockWindows[i + 1];
          cadenaMock += '(' + elemento + ' = (' + elemento + ' || {};\n';
          i++;
        }
      }
    }
  }
  return cadenaMock;
}

export function convertirMayusEnMinus(cadena: string): string {
  return cadena.replace(/^[A-Z]/, (match) => match.toLowerCase());
};


//desde aqui son los nuevos mocks para los spyOn con el Ast , usando json como mocks en la nueva arquitectura

//poner el de mas de un elemento de mocks 
export function mockObject(propertySearch:string):string{
  let result='';
  const arrayFilesIsObject:StandardizedFilePath[] = analysisFileJson(propertySearch);
  if (arrayFilesIsObject.length !== 0) {
    result +=`const mock${propertySearch} = require('${arrayFilesIsObject[0]}')`;
  }
  return result;

}
