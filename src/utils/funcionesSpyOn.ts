import {
  ClassDeclaration,
  FunctionDeclaration,
  MethodDeclaration,
  Node,
  PropertyDeclaration,
  ts
} from 'ts-morph';
import { llamadasMetodos } from './condicionales/funcionesCondicionales';
import { getVariableName, modifiersAccess } from './funcionesAst';


export function generateMethodCall(clase: ClassDeclaration | undefined, metodo: MethodDeclaration | FunctionDeclaration | PropertyDeclaration, parametrosN: string[] = []) {
  let sintaxisComponent: string = '';
  const variableName = getVariableName(clase);
  sintaxisComponent += variableName;
  if (modifiersAccess(metodo, ts.SyntaxKind.PrivateKeyword)) {
    sintaxisComponent += `['${metodo.getName()}']`;
  } else if (clase?.getName()) {
    sintaxisComponent += `.${metodo.getName()}`;
  } else {
    sintaxisComponent += `${metodo.getName()}`;
  }
  if (parametrosN?.length) {
    sintaxisComponent += '(' + parametrosN + ');';
  } else {
    sintaxisComponent += '();';
  }
  return sintaxisComponent;
}

//ast nuevo





export function claseLlamada(metodo: MethodDeclaration | FunctionDeclaration): string {
  let resultado: string = '';
  const arraydescendientes: Node[] = llamadasMetodos(metodo);
  for (const call of arraydescendientes) {
    const clase: Node[] = call.getDescendants();
    if (clase[1].getKindName() === 'Identifier') {
      resultado = clase[1].getText();
    } else {
      resultado = `${clase[3].getText()}`;
    }
  }
  return resultado;
}
