import { FunctionDeclaration, MethodDeclaration, Node, ParameterDeclaration, SourceFile, Symbol, Type } from 'ts-morph';
import { SampleValueGenerator } from '../generators/SampleValueGenerator';
import { añadirImportEnum, listaTypado } from './constants';
import { convertirNodeAMethodOrParam, modifiersAccess, typadoAlias } from './funcionesAst';
import { convertirMayusEnMinus } from './funcionesMocks';
//tipos para en caso de que sea un objeto el parametro todas sus propiedades.

export function typeIsVuelo(parametro: ParameterDeclaration) {
  let testMetodo = '';
  if (parametro.getType().getText().startsWith('{')) {
    const propiedadesObject = parametro.getType().getProperties();
    testMetodo = `\n\tconst ${parametro.getName()}:${parametro.getType().getText()}={`;
    for (const propiedades of propiedadesObject) {
      const nombrePropiedades = propiedades.getName();
      const typadoPropiedades = propiedades.getValueDeclaration()?.getType();
      if (!typadoPropiedades) {
        continue;
      }
      testMetodo += `${nombrePropiedades}:${typadoAst(nombrePropiedades, typadoPropiedades)},`;
    }
    testMetodo += '};';
  }
  return testMetodo;
}

function typedIsArrayElementType(typado: Type): string {
  let result: string = '';
  if (typado.isArray()) {
    result = '[';
    const elementTypeArray: Type | undefined = typado.getArrayElementType();
    if (elementTypeArray !== undefined) {
      result += typadoAst('', elementTypeArray);
    }
    result += ']';
  }
  return result;
}

export function typedArguments(typado: Type): string {
  let result: string = '';
  const arraytypeArguments: Type[] = typado.getTypeArguments().map(element => element);
  if (arraytypeArguments) {
    for (const argumentos of arraytypeArguments) {
      result = typadoAst('', argumentos);
    }
  }
  return result;
}

export function typadoAliasGlobal(sourceFile: SourceFile) {
  let resultado = '';
  const arrtypado = typadoAlias(sourceFile);
  if (arrtypado) {
    for (const typado of arrtypado) {
      const propiedades = typado.getProperties();
      if (propiedades) {
        resultado = '{';
        for (const propiedad of propiedades) {
          const typadoPropiedad = propiedad.getValueDeclaration()?.getType();
          const nombrePropiedad = propiedad.getName();
          if (!typadoPropiedad) {
            continue;
          }
          resultado += `\n\t\t\t\t${nombrePropiedad}:${typadoAst(nombrePropiedad, typadoPropiedad)},`;
        }
        resultado += '}';
      } else {
        resultado += `${typadoAst('', typado)},`;
      }
    }
  }
  return resultado;
}

let interfacesProcesadas = new Set();

export function typadoInterfaz(typado: Type): string {
  let resultado = '';
  if (listaTypado.includes(typado.getText())) {
    resultado = `new ${typado.getText()}()`;
  } else if (typado.getText() === 'Function') {
    resultado = `() => {}`;
  } else {
    interfacesProcesadas.add(typado.getText());
    const propiedadesInterface = typado.getProperties();
    resultado = '{';
    for (const propiedad of propiedadesInterface) {
      const typadoPropiedad = propiedad.getValueDeclaration()?.getType();
      if (!typadoPropiedad) {
        continue;
      }
      if (!propiedad.isOptional()) {
        if (interfacesProcesadas.has(typadoPropiedad.getText())) {
          resultado += `${propiedad.getName()}:{},\n\t\t\t\t`;
          continue;
        } else {
          resultado += `${propiedad.getName()}:${typadoAst(propiedad.getName(), typadoPropiedad)},\n\t\t\t\t`;
        }
      }
    }
    resultado += '}';
  }
  return resultado;
}

function importTypadoEnum(typado: Type): string {
  let resultado = '';
  const ruta = typado
    .getText()
    .replace(/import\(|\).(\w+)|"/gs, '')
    .replace(/(.+)\/src/gs, 'src');
  const nombreVariable = typadoEnum(typado).replace(/\.(\w+)/gs, '');
  resultado += `import { ${nombreVariable} } from '${ruta}'`;
  return resultado;
}

export function typadoEnum(typado: Type): string {
  let resultado = '';
  const variableEnum = typado.getText().replace(/import\(.+\)\./, '');
  let enumeracion = typado.getSymbol()?.getDeclarations()[0].getSourceFile().getEnums();
  enumeracion = enumeracion?.filter(element => element.getName() === variableEnum);
  const membersEnum = enumeracion?.[0].getMembers()[0].getName();
  if (variableEnum && membersEnum) {
    resultado = `${variableEnum}.${membersEnum}`;
  }
  return resultado;
}

export function typadoObjectLiteral(typado: Type): string {
  let resultado = '';
  const textoObject = typado.getText();
  if (textoObject.endsWith('void') || typado.isVoid()) {
    resultado = 'jest.fn()';
    return resultado;
  }
  if (textoObject.startsWith('{') && textoObject.endsWith('}')) {
    resultado = '{';
    if (typado.getProperties()) {
      for (const propiedad of typado.getProperties()) {
        const typadoPropiedad = propiedad.getValueDeclaration()?.getType();
        const nombrePropiedad = propiedad.getName();
        if (!typadoPropiedad) {
          continue;
        }
        resultado += `${nombrePropiedad}:${typadoAst(nombrePropiedad, typadoPropiedad)},`;
      }
    }
    resultado += '}';
  }
  return resultado;
}

export function typoObjectRecord(typado: Type): string {
  let resultado = '';
  resultado = '{';
  if (typado.getTargetType()?.getAliasSymbol()?.getName() === 'Record') {
    resultado += '\n\t\t\t';
    const aliasType = typado.getAliasTypeArguments();
    if (aliasType.length === 2) {
      if (typado.getProperties()) {
        for (const propiedad of typado.getProperties()) {
          const nombrePropiedad = propiedad.getName();
          resultado += `${nombrePropiedad}:${typadoAst(nombrePropiedad, aliasType[1])},\n\t\t\t\t\t`;
        }
        resultado += '}';
      }
    }
  } else {
    resultado += '}';
  }
  return resultado;
}

function oneTypadoUnion(typado: Type): Type {
  let oneTypoUnion: Type = typado;
  const aTypadoUnion: Type[] = typado.getUnionTypes();
  for (const typadoOne of aTypadoUnion) {
    if (typadoOne.getText() !== 'undefined') {
      oneTypoUnion = typadoOne;
      break;
    }
  }
  return oneTypoUnion;
}
function typadoUnion(typado: Type): string {
  let resultado = '';
  resultado = typadoAst('', oneTypadoUnion(typado));
  return resultado;
}

function propertiesClass(arrayPropertiesClass:Symbol[]):string{
  let result:string='';
  for (const property of arrayPropertiesClass) {
    const propertyName:string = property.getName();
    const valueDeclaration:Node|undefined = property.getValueDeclaration();
    result += `${propertyName}`;
    if (valueDeclaration) {
      const convertirValueInMethod: MethodDeclaration | undefined = convertirNodeAMethodOrParam(valueDeclaration);
      if (convertirValueInMethod) {
        if (modifiersAccess(convertirValueInMethod, 128)) {
          result += `: () =>{}`;
        } else{
          const typadoPropiedad = valueDeclaration.getType();
         if (!property.isOptional) {
          if (interfacesProcesadas.has(typadoPropiedad?.getText())) {
            return `CIRCULAR_REFERENCE {}as ${typadoPropiedad.getText().replace(/import(.+)\./gm, '')}`;
          } else {
            result += `${propertyName}}:${typadoAst(propertyName, typadoPropiedad)},\n\t\t\t\t`;
          }
         }
        }
      }
    }
  }
  return result;
}

export function typadoClass(typado: Type): string {
  let resultado:string = '';
  interfacesProcesadas.add(typado.getText());
  if (typado.isClass()) {
    const arrayPropertiesClass:Symbol[] = typado.getProperties();
    if (arrayPropertiesClass) {
      resultado = `{ `;
      resultado+=propertiesClass(arrayPropertiesClass);
      resultado += ` }`;
    } else {
      resultado = `new ${typado.getText().replace(/import(.+)\./gm, '')}()`;
    }
  }
  return resultado;
}
export function typadoAst(nombre: string, typado: Type | undefined): string {
  let resultado: string;
  if (!typado) {
    return '';
  }
  if (typado.isString()) {
    resultado = `'${nombre}'`;
  } else if (typado.isStringLiteral()) {
    resultado = `${typado.getText()}`;
  } else if (typado.isNumber() || typado.isNumberLiteral()) {
    resultado = '1';
  } else if (typado.isBoolean() || typado.isBooleanLiteral()) {
    resultado = 'true';
  } else if (typado.isArray()) {
    resultado = typedIsArrayElementType(typado);
  } else if (typado.isAny()) {
    resultado = "'any'";
  } else if (typado.isUnknown()) {
    resultado = `'${nombre}'`;
  } else if (typado.isVoid()) {
    resultado = `() => {}`;
  } else if (typado.isEnum() || typado.isEnumLiteral()) {
    resultado = `${typadoEnum(typado)}`;
    añadirImportEnum.add(importTypadoEnum(typado));
  } else if (typado.isUnion()) {
    resultado = typadoUnion(typado);
  } else if (typado.isObject() && !typado.isClassOrInterface()) {
    resultado = `${typadoObjectLiteral(typado)}` || `${typoObjectRecord(typado)}`;
  } else if (typado.isClass()) {
    console.log(typado.getText());
    resultado = typadoClass(typado);
  } else if (typado.isInterface()) {
    resultado = typadoInterfaz(typado);
  } else {
    resultado = `'${nombre}'`;
  }
  return resultado;
}

function typeParamLimitAst(parametro: ParameterDeclaration): string {
  let result: string = '';
  const typeParam: string = parametro.getText();
  if (RegExp(/:\s+(\w+)\.(\w+)/gm).test(typeParam)) {
    const match: RegExpMatchArray | null = typeParam.match(/(\w+)?\./gm);
    if (match) {
      match.map(element => (result += element));
    }
  }
  return result;
}

export function typadoParametro(parametro: ParameterDeclaration): string {
  let resultado = '';
  const typadoParam = parametro.getType();
  if (typadoObjectLiteral(typadoParam)) {
    resultado = `{\n\t\t`;
    if (typadoParam.getProperties()) {
      for (const propiedad of typadoParam.getProperties()) {
        const typadoPropiedad = propiedad.getValueDeclaration()?.getType();
        const nombrePropiedad = propiedad.getName();
        resultado += `${nombrePropiedad}:${typeParamLimitAst(parametro)}${typeParamLimitAst(parametro)}${typadoPropiedad?.getText().replace(/import\(.+\)\./, '')},\n\t\t`;
      }
    }
    resultado += '}';
  } else if(typadoParam.isBoolean()){
      resultado= `boolean`;
  } else if (typadoParam.isUnion() && !parametro.isOptional()) {
    resultado =
      typeParamLimitAst(parametro) +
      oneTypadoUnion(typadoParam)
        .getText()
        .replace(/import\(.+\)\./, '')
        .replace(/undefined|\|/gs, '')
        .trim();
  } else {
    resultado =
      typeParamLimitAst(parametro) +
      typadoParam
        .getText()
        .replace(/import\(.+\)\./, '')
        .replace(/undefined|\|/gs, '')
        .trim();
  }
  return resultado;
}

export function objetoParametroMetodo(
  metodo: MethodDeclaration | FunctionDeclaration,
  sourceFile: SourceFile
) {
  const parametros = metodo.getParameters().map(parametro => parametro);
  let testMetodo = '';
  const sampleValueGenerator = new SampleValueGenerator();
  for (const parametro of parametros) {
    const variableName = convertirMayusEnMinus(parametro.getName());
    const variableType = typadoParametro(parametro);
    const objectValue = sampleValueGenerator.getSampleValueForType(parametro.getType(), metodo);
    testMetodo += `\nconst ${variableName}: ${variableType} = ${objectValue};`;
    
  }
  return testMetodo;
}
