//funciones para cantidadParametros

import { ClassDeclaration, FunctionDeclaration, MethodDeclaration, Node, ParameterDeclaration, Type } from 'ts-morph';
import { typadoAst } from '../funcionesParametrosDeMetodos';

//funcion que saca una cadena de los parametros que tiene un metodo o una funcion
function arrayParamsSyntax(
  metodo: MethodDeclaration | FunctionDeclaration,
  nombreVariables: string[],
  syntaxis: string
) {
  let resultado = '';
  const arrayParameters: ParameterDeclaration[] = metodo.getParameters();
  arrayParameters.map(parameter => {
    const nombre = nombreVariables?.find(v => v === parameter.getName());
    resultado += nombre ? nombre : syntaxis;
    resultado += ', ';
  });
  return resultado;
}

//funcion que saca si un metodo pertenece a una clase abstracta.
function claseAbstracta(metodo: MethodDeclaration): ClassDeclaration | '' {
  let ancesClase: Node | undefined = metodo
    .getAncestors()
    .find(element => element.getKindName() === 'ClassDeclaration');
  if (ancesClase) {
    ancesClase = ancesClase as ClassDeclaration;
  }
  return ancesClase instanceof ClassDeclaration ? ancesClase : '';
}

//funcion de parametros que son desestructurados es decir un parametro tipo {propiedad,propiedad}, no se tiene en cuenta las propiedades que sean opcionales
export function parametroDesestructurado(typado: Type): string {
  let resultado = '';
  const textoObject = typado.getText();
  if (textoObject.startsWith('{') && textoObject.endsWith('}')) {
    resultado = '{';
    if (typado.getProperties()) {
      for (const propiedad of typado.getProperties()) {
        const nombrePropiedad = propiedad.getName();
        //los elementos opcionales no son necesarios para las clases abstractas.
        if (!propiedad.isOptional()) {
          resultado += `${nombrePropiedad},`;
        }
      }
    }
    resultado = resultado.replace(/,$|,\s+$/gm, '');
    resultado += '}';
  }
  return resultado;
}

//funcion de los parametros de una clase abstracta sean desestructurados o no , las clases abstractas no tienen cast , asi que cuando se llama en los test
//necesita pasarle todos sus parametros o undefined , como pasa con las funciones.
function paramClaseAbs(nombreVariables: string[], arrayParameters: ParameterDeclaration[]): string {
  let resultado = '';
  resultado += arrayParameters.map(element => {
    if (parametroDesestructurado(element.getType())) {
      return parametroDesestructurado(element.getType());
    }
  });
  if (!resultado) {
    const tamañoNVar = nombreVariables?.length;
    resultado += `${nombreVariables?.map(element => element)}`;
    if (tamañoNVar < arrayParameters.length) {
      arrayParameters.slice(tamañoNVar).map(element => {
        resultado += `, ${typadoAst('', element.getType())}`;
      });
    }
  }
  return resultado;
}

//funcion que saca la cantidad de parametros que se le añadira a la llamada del metodo o funcion para ese nuevo test del stataments que se esta analizando
//comportamiento diferente segun sea un metodo ya que en este caso tenemos cast , o es una funcion , o tenemos una clase abstracta
export function cantidadParametros(metodo: MethodDeclaration | FunctionDeclaration, nombreVariables: string[]) {
  let resultado: string = '';
  const clase: ClassDeclaration | '' = claseAbstracta(metodo as MethodDeclaration);
  const arrayParameters: ParameterDeclaration[] = metodo.getParameters();
  if (metodo.getKindName() === 'FunctionDeclaration') {
    const syntax = `undefined `;
    resultado = arrayParamsSyntax(metodo, nombreVariables, syntax);
  } else if (clase instanceof ClassDeclaration && clase.isAbstract()) {
    resultado = paramClaseAbs(nombreVariables, arrayParameters);
  } else if (metodo.getKindName() === 'MethodDeclaration' && nombreVariables?.length) {
    resultado = `${nombreVariables.map(element => element)},`;
  }
  resultado = resultado.replace(/,$|,\s+$/gm, '');

  return resultado;
}
