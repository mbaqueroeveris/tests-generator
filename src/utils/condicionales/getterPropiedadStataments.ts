import { Node, Type } from 'ts-morph';
import { convertirMayusEnMinus } from '../funcionesMocks';
import { typadoAst } from '../funcionesParametrosDeMetodos';

// metodos de procesarAccesoPropiedad anidacion de los elementos PropertyAccessExpression
function arrayNodesIdentifiers(node: Node): Node[] {
  const arrayNodesIndentifiers = node.forEachDescendantAsArray().filter(node => node.getKindName() === 'Identifier');
  const arraySinLenght = arrayNodesIndentifiers.filter(node => node.getText() !== 'length');
  return arraySinLenght;
}

function obtenerPropertyAccessExpression(nodo: Node): Node[] | [] {
  if (nodo.getKindName() !== 'PropertyAccessExpression') {
    return [];
  }
  return arrayNodesIdentifiers(nodo);
}

function generarTipo(oneElement: boolean, nombrePropiedad: string, tipo: Type): string {
  const tipoElementoArray: Type | undefined = tipo.getArrayElementType();
  if (oneElement) {
    if (tipo.isArray()) {
      return `${nombrePropiedad}:[${typadoAst('', tipoElementoArray)}]`;
    } else {
      return `${nombrePropiedad}:${typadoAst(nombrePropiedad, tipo)}`;
    }
  } else {
    if (tipo.isArray()) {
      return `const ${nombrePropiedad}=[${typadoAst('', tipoElementoArray)}]`;
    } else {
      return `const ${nombrePropiedad}=${typadoAst(nombrePropiedad, tipo)}`;
    }
  }
}

function typoPropertyInConditional(node: Node): string {
  const prueba: string[] | undefined = node
    ?.getSymbol()
    ?.getDeclarations()
    .map(element => element.getKindName());
  if (!prueba?.includes('Parameter')) {
    return prueba?.[0] || '';
  } else {
    return '';
  }
}

function isDescontruccionArrayBinding(node: Node) {
  let resultado = '';
  const typoProperty = typoPropertyInConditional(node);
  if (typoProperty === 'BindingElement') {
    //elemento vinculante caso especial
    let variableDestructurada = node.getType().getSymbol()?.getName() ?? '';
    variableDestructurada = convertirMayusEnMinus(variableDestructurada);
    const typadoVDes = node.getType();
    const abrir = typadoVDes.isArray() ? '[{' : typadoVDes.isObject() ? '{' : "'";
    resultado += `${variableDestructurada}:[${abrir}`;
  }
  return resultado;
}
function cierreSintaxis(node: Node) {
  let cierre = '';
  if (isDescontruccionArrayBinding(node)) {
    cierre = ']}';
  }
  return cierre;
}

function anadirElementIsNoParameter(node: Node): string {
  let resultado = '';
  if (isDescontruccionArrayBinding(node)) {
    resultado = isDescontruccionArrayBinding(node);
  }
  return resultado;
}

function generarAccesoPropiedad(propiedades: Node[], cierres: string, valorType?: string): string {
  let resultado: string = '';
  let abrir: string = '';
  let unElement: boolean = false;
  let cerrarUltimo: string = '';
  for (let i = 0; i < propiedades.length; i++) {
    const nombrePropiedad: string = propiedades[i].getText();
    const tipoPropiedad: Type = propiedades[i].getType();
    if (i === 0 && propiedades.length !== 1) {
      cerrarUltimo = tipoPropiedad.isArray() ? '}]' : tipoPropiedad.isObject() ? '}' : "'";
      abrir = tipoPropiedad.isArray() ? '[{' : tipoPropiedad.isObject() ? '{' : "'";
      resultado += `const ${nombrePropiedad}=${abrir}`;
      resultado += `${anadirElementIsNoParameter(propiedades[i])}`;
      cerrarUltimo += cierreSintaxis(propiedades[i]);
    } else if (propiedades.length === 1) {
      resultado += generarTipo(unElement, nombrePropiedad, tipoPropiedad);
      resultado += `${anadirElementIsNoParameter(propiedades[i])}`;
      resultado += cierreSintaxis(propiedades[i]);
    } else if (i === propiedades.length - 1) {
      if (valorType) {
        resultado += `${nombrePropiedad}:${valorType}`;
      } else {
        unElement = true;
        resultado += generarTipo(unElement, nombrePropiedad, tipoPropiedad);
      }
      resultado += `${cierres}${cerrarUltimo}`;
    } else {
      abrir = tipoPropiedad.isArray() ? '[{' : tipoPropiedad.isObject() ? '{' : "'";
      cierres += tipoPropiedad.isArray() ? '}]' : tipoPropiedad.isObject() ? '}' : "'";
      resultado += `${nombrePropiedad}:${abrir}`;
    }
  }
  resultado += `;\n\t\t`;
  return resultado;
}
//metodo que obtiene la cadena de un elemento del statatment que sea PropertyAccessExpression, aplicando las anteriores funciones si se cumplen.
export function procesarAccesoPropiedad(operando: Node, valorType?: string): string {
  let cierres: string = '';
  if (obtenerPropertyAccessExpression(operando).length !== 0) {
    const properties: Node[] = obtenerPropertyAccessExpression(operando);
    return generarAccesoPropiedad(properties, cierres, valorType);
  } else {
    return '';
  }
}
