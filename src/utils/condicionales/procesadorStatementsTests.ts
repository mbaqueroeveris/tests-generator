import { Node, Type } from 'ts-morph';
import { typadoAst } from '../funcionesParametrosDeMetodos';
import { procesarAccesoPropiedad } from './getterPropiedadStataments';

//interfaz exportada NodeChain valores opcionales utilizados en algunos metodos esta interfaz creada.
export interface NodeChain {
  nodoAnterior?: Node;
  nodoSiguiente?: Node;
}

//parametros de la condicion que son repetidos, se sustituye los elementos repetidos por un operador logico para unirlos
function parametrosRepeCondicion(cadena: string): string {
  const propiedades = new Set();
  cadena = cadena.replace(/(\w+):|,/g, (match, p1) => {
    if (propiedades.has(p1)) {
      return '&&';
    } else {
      propiedades.add(p1);
      return match;
    }
  });
  return cadena;
}

//funcion de operadorLogico Or si tenemos algun descendiente que sea ||
function operadorLogicoBar(insideNode: Node): Node | undefined {
  const barLogico: Node | undefined = insideNode.forEachDescendant(node => {
    if (node.getKindName() === 'BarBarToken') {
      const nodoAnterior: Node | undefined = node.getPreviousSibling();
      return nodoAnterior;
    }
  });
  if (barLogico) {
    return barLogico ?? undefined;
  }
}
//lo mismo para los operadores && con la diferencia de que retorno un objeto con dos propiedades , una sera el elemento anterior del && y otro el siguiente
function operadoresAndLogicos(insideNode: Node): NodeChain | undefined {
  const andLogico: NodeChain | undefined = insideNode.forEachDescendant(node => {
    if (node.getKindName() === 'AmpersandAmpersandToken') {
      const nodoAnterior: Node | undefined = node.getPreviousSibling();
      const nodoSiguiente: Node | undefined = node.getNextSibling();
      return { nodoAnterior, nodoSiguiente };
    }
  });
  if (andLogico) {
    return andLogico ?? undefined;
  }
}

//funcion que trabaja con los anteriores dos metodos de operadores y genera una cadena llamando a ValorFinalCondicional y ProcesarAccesoPropiedad
export function cadenaOperadorLogico(insideNode: Node): string {
  let cadenaOpe: string = '';
  const operadorAndLogico: NodeChain | undefined = operadoresAndLogicos(insideNode);
  const operadorBarLogico: Node | undefined = operadorLogicoBar(insideNode);
  if (operadorAndLogico !== undefined) {
    const nodePrevious: Node | undefined = operadorAndLogico.nodoAnterior;
    const nodeNext: Node | undefined = operadorAndLogico.nodoSiguiente;
    if (nodeNext !== undefined && nodePrevious !== undefined) {
      cadenaOpe = procesarAccesoPropiedad(nodePrevious);
      cadenaOpe += ValorFinalCondicional(nodeNext,typadoAst);
      cadenaOpe = parametrosRepeCondicion(cadenaOpe);
    }
  }
  if (operadorBarLogico !== undefined) {
    cadenaOpe += procesarAccesoPropiedad(operadorBarLogico);
  }
  return cadenaOpe;
};

//funcion para los elementos que sean === dentro de un statament , obtenemos tambien el elemento anterior que nos interesa si es identifier o PropertyAccessExpression y el valor que necesita este
export function elementosEqualsconditionals(descendiente: Node): string {
  let valorCompa: string = '';
  const nodoEquals: NodeChain | undefined = descendiente.forEachDescendant(node => {
    if (node.getKindName() === 'EqualsEqualsEqualsToken') {
      const nodoAnterior = node.getPreviousSibling();
      const nodoSiguiente = node.getNextSibling();
      return { nodoAnterior, nodoSiguiente };
    }
  });
  if (nodoEquals?.nodoAnterior && nodoEquals?.nodoSiguiente?.getText()) {
    const nodoIndetifier: Node = nodoEquals.nodoAnterior;
    const valor: string = nodoEquals?.nodoSiguiente?.getText();
    const valorProcesPropiedad: string = procesarAccesoPropiedad(nodoIndetifier, valor);
    if (valorProcesPropiedad) {
      valorCompa += `${valorProcesPropiedad}`;
    } else {
      valorCompa += `const ${nodoIndetifier.getText()}=${valor};\n\t\t`;
    }
  }
  return valorCompa;
}

//si el elemento siguiente al identifier es un ( significa que el statament es identifier(identifier), lo que nos interesa es lo que hay dentro del parentesis
//esta funcion obtenemos los decendientes dentro de este parentesis
export function idenInsideParen(iParen: Node): Node | '' {
  if (iParen.getNextSiblingIfKind(21)) {
    const nodoIParen = iParen.getNextSibling()?.getNextSibling();
    const nodoInseIden = nodoIParen?.getDescendantsOfKind(80);
    return nodoInseIden?.[0] ?? '';
  } else {
    return '';
  }
}

/*function propertyAccessExpressionDes(inside:Node):Node|undefined{
  if (inside !== undefined) {
    const nodoProperty: Node | undefined = inside.forEachDescendant(node => {
      if (node.getKindName() === 'PropertyAccessExpression') {
        return node;
      }
    });
    return nodoProperty;
}
};*/
function findDescen(inside:Node):Node | undefined{
  if (!inside) {return undefined;}  
  let nodeInside = inside.forEachDescendant(node => {
    if (node.getKindName() === 'Identifier' || node.getKindName() === 'PropertyAccessExpression') {
      return node;
    }
  });
  return nodeInside;
};


//funcion que se exporta , con esta funcion obtenemos la cadena final de ese statament aplicando los diferentes funciones para cubrir de forma automatica
//los operadores, identifiers, PropertyAccessExpression etc.. del statament y este resultado sera lo que hagamos en cada test añadido.
export function ValorFinalCondicional(inside: Node,syntax:Function): string {
  let resultado: string = '';
  let nodeInside:Node|undefined;
  if (!inside) {return '';}

    //if(propertyAccessExpressionDes(inside)===undefined||identifierDescen(inside)===undefined){
      //nodeInside=inside;
    //}*/
    // Combine identifier and property access checks for efficiency
  if(findDescen(inside)!==undefined){
    nodeInside=findDescen(inside);
  }else{
    console.log("haber si funciona");
    nodeInside=inside;
  }
    if(nodeInside!==undefined){
      if (nodeInside.getKindName()==='Identifier') {
        const idenInside = idenInsideParen(nodeInside);
        if (idenInside) {
          nodeInside = idenInside;
        }
        const nombreNodo: string = nodeInside.getText();
        const typadoNode: Type = nodeInside.getType();
        const elementEquals: string = elementosEqualsconditionals(inside);
        if (elementEquals) {
        resultado = elementEquals;
        } else {
          if (nodeInside.getKindName() !== 'PropertyAccessExpression') {
            resultado += `const ${nombreNodo}= ${syntax(nombreNodo, typadoNode)};\n\t\t`;
          }
        }
      }else{
        resultado += procesarAccesoPropiedad(nodeInside);
      }
      if (cadenaOperadorLogico(inside)) {
        resultado = cadenaOperadorLogico(inside);
      }
    }
  return resultado;
};
