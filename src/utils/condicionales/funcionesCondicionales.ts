import {
  ClassDeclaration,
  FunctionDeclaration,
  MethodDeclaration,
  Node,
  ParameterDeclaration,
  Project,
  Statement,
  Type,
  TypeChecker,
  ts,
} from 'ts-morph';
import { listaBlancaSpyOn } from '../constants';
import { typadoAst } from '../funcionesParametrosDeMetodos';
import { generateMethodCall } from '../funcionesSpyOn';
import { cantidadParametros } from './parametrosStataments';
import { ValorFinalCondicional } from './procesadorStatementsTests';

//funciones comentadas , utilizarlas si es neceario solo
/*function herenciaElmentConditional(hijo: Node) {
  let contador = 0;
  let padre = hijo.getParent();
  while (padre) {
    if (padre.getKindName() === 'IfStatement' && !padre.getPreviousSiblingIfKind(ts.SyntaxKind.ElseKeyword)) {
      break;
    }
    contador++;
    padre = padre.getParent();
  }
  return padre;
}

//esto mejorar un poco la logica del mismo .
function aislamientoHerencia(contadorIf: number, stringCondicionales: string[]): string {
  const aislarElementos: string[] = stringCondicionales[stringCondicionales.length - contadorIf].split(/\;/gs);
  aislarElementos.pop();
  aislarElementos.pop();
  const oneElementPadre: string = aislarElementos.join(';');
  return oneElementPadre;
}
*/
//funciones de remplazar repetidos para unir variables que sean iguales en un mismo object
export function remplazarRepetidos(conditionales: string): string[] {
  let sinRepetidos: string[] = [];
  if (detectarRepeticionesConst(conditionales)) {
    sinRepetidos.push(detectarRepeticionesConst(conditionales));
  } else {
    sinRepetidos.push(conditionales);
  }
  sinRepetidos = sinRepetidos.map(element => element.replace(/\,\&\&/g, '&&'));
  sinRepetidos = sinRepetidos.map(element => element.replace(/\,\s+\,/gs, ','));
  sinRepetidos = sinRepetidos.map(element => element.replace(/\,const/gs, 'const'));
  return sinRepetidos;
};

//por aqui empiezo con los else;
//function 

function clavesObjectRepe(arrayObjeto: string[]): string[] {
  const clavesUsadas = new Set();
  let elementosFiltrados: string[] = arrayObjeto.filter(elemento => {
    const clave: string = elemento.split(':')[0];
    if (clavesUsadas.has(clave)) {
      return true;
    }
    clavesUsadas.add(clave);
    return false;
  });
  return elementosFiltrados;
}

function seRepite(variable: string, objectString: string[]): string {
  let resultadoFinal: string = '';
  let elementosNoRepet: string[] = [];
  let objetoCombinado: string = '';
  objetoCombinado += `const ${variable}={`;
  const objetosRelevantes: string[] = objectString.filter(element => element.includes(variable));
  elementosNoRepet = objectString.filter(element => !element.includes(variable));
  const onlyObjects: string[] = objetosRelevantes.map(objecto => objecto.replace(/const\s+(\w+)=|\{|\}/gm, ''));
  if (clavesObjectRepe(onlyObjects)[0] !== undefined) {
    const sinClavesRepe: string[] = clavesObjectRepe(onlyObjects);
    objetoCombinado += sinClavesRepe.map(element => `${element}`);
  } else {
    objetoCombinado += onlyObjects.map(element => `${element}`);
  }
  objetoCombinado += `};\n\t\t`;
  resultadoFinal += objetoCombinado;
  resultadoFinal += elementosNoRepet.map(element => `${element};\t\t\n`);
  return resultadoFinal;
}

function detectarRepeticionesConst(cadena: string): string {
  let resultado: string = '';
  const variableRepetida = new Set();
  cadena += ';';
  const variables: string[] = cadena.match(/const\s+(\w+)/g)?.map(element => element.replace(/const\s+/gs, '')) ?? [];
  const objectString: string[] = cadena
    .replace(/\n|\t/gs, '')
    .split(/;/gs)
    .filter((objeto: string) => objeto !== '');
  for (const variable of variables) {
    if (variableRepetida.has(variable)) {
      resultado = seRepite(variable, objectString);
    } else {
      variableRepetida.add(variable);
    }
  }
  return resultado;
}

//metodos mas test
export function condicionalIf(metodo: MethodDeclaration | FunctionDeclaration): Node[] {
  const arrayStatements: Statement[] = metodo.getStatements();
  let conditional: Node[] = [];
  for (const statament of arrayStatements) {
    if (statament.getKind() === ts.SyntaxKind.IfStatement) {
      conditional = statament.getDescendants();
    }
  }
  return conditional;
}

function syntaxfunctionMetodo(metodo: MethodDeclaration | FunctionDeclaration): string {
  let resultado: string = '';
  const clase = metodo.getFirstAncestorByKind(ts.SyntaxKind.ClassDeclaration);
  if (metodo.getKindName() === 'MethodDeclaration') {
    resultado = generateMethodCall(clase, metodo);
  } else if (metodo.getKindName() === 'FunctionDeclaration') {
    resultado = '';
  }
  return resultado;
}

export function mastest(metodo: MethodDeclaration | FunctionDeclaration): string {
  let resultado: string = '';
  let nombresVariables: string[] = [];
  let contador: number = 2;
  let variables: string = '';
  if (condicionalIf(metodo)?.[0] !== undefined) {
    //const arrayConditional: Node[] = condicionalIf(metodo);
    //const stringCondicionales: string[] = ifAnidados(arrayConditional);
    //if (ifAnidados(arrayConditional)[0] === undefined) {
    const arrayConditional: (Node | undefined)[] = nodeIfStatamentAnidacion(metodo);
    console.log(nodeElseStatament(metodo));
    for (const conditional of arrayConditional) {
      if (conditional !== undefined) {
        variables += ValorFinalCondicional(conditional,typadoAst);
      } else {
        variables = remplazarRepetidos(variables).join('\t');
        nombresVariables = variables.match(/const\s+(\w+)/g)?.map(element => element?.replace(/const\s+/gs, '')) ?? [];
        const cantParam: string = cantidadParametros(metodo, nombresVariables);
        resultado += escribirTest(metodo, variables, cantParam, contador);
        contador++;
        variables = '';
      }
    }
    //llevar esto a la anidacion para tener en cuenta los padres para los else anidados , esto es complejo , por ahora se ejecuta los else pero sin anidacion
    const arrayElementElse:string[]=nodeElseStatament(metodo);
    arrayElementElse.map(element=>{
      variables=element;
      nombresVariables = variables.match(/const\s+(\w+)/g)?.map(element => element?.replace(/const\s+/gs, '')) ?? [];
      const cantParam: string = cantidadParametros(metodo, nombresVariables);
        resultado += escribirTest(metodo, variables, cantParam, contador);
        contador++;
    });
    /* } else {
      for (const variables of stringCondicionales) {
        nombresVariables = variables.match(/const\s+(\w+)/g).map(element => element.replace(/const\s+/gs, ''));
        const cantParam: string = cantidadParametros(metodo, nombresVariables);
        resultado += escribirTest(metodo, variables, cantParam, contador);
        contador++;
        }
      */
  }
  //}
  return resultado;
}

export function typadoElseNode(nombre:string,elseType:Type):string{
    let resultado: string;
    if (elseType.isString()) {
      resultado = `null`;
    } else if (elseType.isStringLiteral()) {
      resultado = `''`;
    } else if (elseType.isNumber() || elseType.isNumberLiteral()) {
      resultado = 'NaN';
    } else if (elseType.isBoolean() || elseType.isBooleanLiteral()) {
      resultado = 'false';
    } else if (elseType.isArray()) {
      resultado = '[]';
    }else if(elseType.isObject()){
      resultado= `{}`;
    } else {
      resultado = `''`;
    }
    return resultado;
};

//saco los else de un metodo entero despues sacar los else de los if si los tiene 
function nodeElseStatament(method:MethodDeclaration|FunctionDeclaration):string[]{
  const arrayElementElse:string[]=[];
  const arrayElseStatament:(Node|undefined)[]=method.getDescendants().filter(element=>
    element.getKindName()==='ElseKeyword' && element.getNextSiblingIfKind(ts.SyntaxKind.Block))
  .map(element=>{
    if(element.getParent()?.getKindName() === 'IfStatement'){
        return element.getParent();
    }});
    for(const elseElement of arrayElseStatament){
      if(elseElement!==undefined){
        const valorElse:string =ValorFinalCondicional(elseElement.getChildren()[2],typadoElseNode);
        arrayElementElse.push(valorElse);
      }
  }
  return arrayElementElse;
};

//hasta los metodos para mas test
//array de nodos con anidacion, probar sustituir mañana en los otros if
function nodeIfStatamentAnidacion(method: MethodDeclaration | FunctionDeclaration) {
  const condicionales: (Node | undefined)[] = [];
  method.forEachDescendantAsArray().map(node => {
    if (node.getKindName() === 'IfStatement') {
      const ifAnidado = node.getAncestors().some(ancestors => ancestors.getKindName() === 'IfStatement');
      if (ifAnidado === true) {
        condicionales.push(node);
       const ifPadre = node.getAncestors().filter(node => node.getKindName() === 'IfStatement');
        const pureba = ifPadre[0].getAncestors().filter(node=>{
          if(node.getKindName()==='ElseKeyword')
          {return node;}
        });
        console.log(pureba[0]?.getText());
        ifPadre.map(element => condicionales.push(element));
        condicionales.push(undefined);
      }
    }
  });
  const insideCondicion: (Node | undefined)[] = condicionales.map(element => {
    if (element !== undefined) {
      return element.getChildren()[2];
    } else {
      return undefined;
    }
  });
  return insideCondicion.map(element => element);
}

//array de nodos sin anidacion implenentar en un futuro a masTest
function nodeIfStatamentSinAnidacion(method: MethodDeclaration | FunctionDeclaration): Node[] {
  const condicionales: Node[] = method.forEachDescendantAsArray().filter(node => {
    if (node.getKindName() === 'IfStatement') {
      const ifAnidado: boolean = node.getAncestors().some(ancestors => ancestors.getKindName() === 'IfStatement');
      if (ifAnidado === false) {
        return true;
      } else {
        return false;
      }
    }
  });
  const insideCondicion: Node[] = condicionales.map(element => element.getChildren()[2]);
  return insideCondicion.map(element => element);
}

function eliminacionDuplicados(arrayCall: Node[]): Node[] {
  const tecnicaSet = new Set();
  const filteredNodes: (Node | undefined)[] = arrayCall.map(node => {
    const nodeText: string = node.getText();
    if (tecnicaSet.has(nodeText)) {
    } else {
      tecnicaSet.add(nodeText);
      return node;
    }
  });
  const filteredArray: Node[] = filteredNodes.filter(element => element !== undefined) as Node[];
  return filteredArray;
}

function filterListaBlancaSpyOn(arrayCall: Node[]): Node[] {
  const arrayListaBlanca: Node[] = arrayCall.filter(element => {
    const elementText: string = element.getText();
    return !listaBlancaSpyOn.some(elementoExcluido => elementText.includes(elementoExcluido));
  });
  return arrayListaBlanca;
}

export function llamadasMetodos(metodo: MethodDeclaration|FunctionDeclaration): Node[] {
  const descendientes: Node[] = metodo.getBody()?.getDescendants() || [];
  const array: Node[] = [];
  for (const desen of descendientes) {
    if (desen.getKind() === ts.SyntaxKind.CallExpression) {
      array.push(desen);
    }
  }
  const arraySinDuplicados: Node[] = eliminacionDuplicados(array);
  const arrayFinalCallsMetodos: Node[] = filterListaBlancaSpyOn(arraySinDuplicados);
  return arrayFinalCallsMetodos;
}

export function descendientesDeLlamadas(metodo: MethodDeclaration|FunctionDeclaration): Node[][] {
  let masdescendientes: Node[][] = [];
  const descendientes: Node[] = llamadasMetodos(metodo);
  for (const desen of descendientes) {
    masdescendientes.push([...desen.getDescendants(), desen]);
  }
  return masdescendientes;
}

export function typadoCalls(metodo: MethodDeclaration|FunctionDeclaration): Type[] {
  const callMetodos: Node[] = llamadasMetodos(metodo);
  const typadoCall: Type[] = callMetodos.map(element => element.getType());
  return typadoCall;
}

//metodos de prueba que aun no tiene uso pero su logica esta implementada
export function returnTypePromise(metodo: MethodDeclaration): boolean {
  let isPromise: boolean = false;
  const project: Project = new Project();
  const typeChecker: TypeChecker = project.getTypeChecker();
  const returnType: Type = typeChecker.getReturnTypeOfSignature(metodo.getSignature());
  if (/Promise/gm.test(returnType.getText())) {
    isPromise = true;
  }
  return isPromise;
}

function escribirTest(
  metodo: MethodDeclaration | FunctionDeclaration,
  variables: string,
  argumentos: string,
  contador: number
): string {
  let resultado: string = '';
  if (variables) {
    resultado += `it("test para ${metodo.getName()}${contador}", () =>{\n\ttry{\n\t\t${variables}
  ${syntaxfunctionMetodo(
    metodo
  )}${metodo.getName()}(${argumentos});\n\t}catch (err){ console.warn('Error in test ${metodo.getName()}${contador}', err);\n}});\n`;
  }
  return resultado;
}

function isArrayReturn(metodo: MethodDeclaration | FunctionDeclaration): ParameterDeclaration[] {
  const porpertyReturn: boolean = metodo.getReturnType().isTypeParameter();
  let arrayParametros: ParameterDeclaration[] = [];
  if (porpertyReturn) {
    arrayParametros = metodo.getParameters().filter(element => element.getType().isArray());
  }
  return arrayParametros;
}

function arrayReturn(metodo: MethodDeclaration | FunctionDeclaration): string {
  let resultado: string = '';
  if (isArrayReturn(metodo)[0] !== undefined) {
    const params: ParameterDeclaration[] = isArrayReturn(metodo);
    for (const param of params) {
      const typoElementArray: Type | undefined = param.getType().getArrayElementType();
      resultado += `const ${param.getName()}:${param.getTypeNode()?.getText()}=[${typadoAst(
        '',
        typoElementArray
      )},${typadoAst('', typoElementArray)}];\n`;
    }
  }
  return resultado;
}

export function otroTestElementArray(metodo: MethodDeclaration | FunctionDeclaration): string {
  let resultado: string = '';
  const variables: string = arrayReturn(metodo);
  let contador: number = 2;
  const argumentos: string = isArrayReturn(metodo)
    .map(element => `${element.getName()},`)
    .join('');
  if (escribirTest(metodo, variables, argumentos, contador)) {
    resultado = escribirTest(metodo, variables, argumentos, contador);
    contador++;
  }
  return resultado;
}

export function propiedadesParam(metodo: MethodDeclaration, clase: ClassDeclaration) {
  const propiedades = metodo.getLocals();
  //.getProperties().map((element)=>element.getName());

  return propiedades;
}
