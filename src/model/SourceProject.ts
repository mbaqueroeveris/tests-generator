import {
  InterfaceDeclaration,
  ParameterDeclaration,
  Project,
  ProjectOptions,
  PropertyDeclaration,
  PropertySignature,
  SourceFile
} from "ts-morph";

export class SourceProject {
  private project: Project;
  private sourceFilesMap: {
    [key: string]: SourceFile[];
  } = {};

  constructor(tsconfigPath: string | undefined = undefined) {
    let options: ProjectOptions | undefined = undefined;
    if (tsconfigPath) {
      options = { tsConfigFilePath: this.sanitize(tsconfigPath) };
    }
    this.project = new Project(options);
    const sourceFiles = this.project.getSourceFiles();
    for (const sourceFile of sourceFiles) {
      this.addSourceToMap(sourceFile);
    }
  }

  public addSource(fileName: string) {
    const sourceFile = this.project.addSourceFileAtPath(fileName);
    this.addSourceToMap(sourceFile);
    return sourceFile;
  }

  public addSourceToMap(sourceFile: SourceFile) {
    const baseName = sourceFile.getBaseNameWithoutExtension();
    if (!this.sourceFilesMap[baseName]) {
      this.sourceFilesMap[baseName] = [];
    }
    this.sourceFilesMap[baseName].push(sourceFile);
  }
  
  public findFile(fileName: string, relativeTo: string | undefined = undefined) {
    const baseName = this.extractBaseName(fileName);
    let sourceFilesFound = this.sourceFilesMap[baseName];
    if (!sourceFilesFound) {
      console.warn("Source not found for file ", fileName);
      try {
        sourceFilesFound = [this.addSource(fileName)];
      } finally {
        // Do nothing
      }
    }
    if (sourceFilesFound) {
      if (sourceFilesFound.length === 1) {
        return sourceFilesFound[0];
      }
      const actualFile = this.sanitize(
        this.findPathRelativeToPath(fileName, relativeTo)
      );
      for (const sourceFile of sourceFilesFound) {
        const filePath = this.sanitize(sourceFile.getFilePath());
        if (filePath.includes(actualFile)) {
          return sourceFile;
        }
      }
    }
    return null;
  }

  public findPathRelativeToPath(
    fileName: string,
    relativeTo: string | undefined = undefined
  ) {
    if (!relativeTo || !fileName.startsWith(".")) {
      return fileName;
    }
    let relativeToPath = relativeTo.replace(/\/[^/]+$/, "/");
    let fileWalker = fileName;
    while (RegExp(/^\.\.\//).exec(fileWalker)) {
      fileWalker = fileWalker.replace("../", "");
      relativeToPath = relativeToPath.replace(/\/[^/]+$/, "/");
    }
    fileWalker = fileWalker.replace(/^\.\//, "");
    return relativeToPath + fileWalker;
  }

  public extractBaseName(filePath: string) {
    let baseName = filePath.replace(/(\.ts|\.js|\.json)$/, "");
    baseName = this.sanitize(baseName);
    baseName = baseName.replace(/^.*\//, "");
    return baseName;
  }

  public sanitize(filePath: string) {
    let sanitizedPath = filePath.replace(/\\/g, "/");
    sanitizedPath = sanitizedPath.replace(/\/\//g, "/");
    sanitizedPath = sanitizedPath.replace(/^[^:]*:/, "");
    return sanitizedPath;
  }


  public findInterfaces(
    parametro: ParameterDeclaration
  ): InterfaceDeclaration[] {
    const parametroType = parametro.getType();
    const typeSymbol = parametroType.getSymbol();
    const declarations = typeSymbol?.getDeclarations();
    const interfaces = [];
    if (declarations) {
      for (const declaration of declarations) {
        if (declaration instanceof InterfaceDeclaration) {
          interfaces.push(declaration);
        }
      }
    }
    return interfaces;
  }

  public findInterfaz(parametro: ParameterDeclaration): InterfaceDeclaration | undefined {
    const interfaces = this.findInterfaces(parametro);
    for (const interfaz of interfaces) {
      if (
        interfaz.getName() === parametro.getType().getSymbol()?.getEscapedName()
      ) {
        return interfaz;
      }
    }
    return undefined;
  }

  public findInterfacesPropiedades(
    parametro: PropertyDeclaration |PropertySignature
  ): InterfaceDeclaration[] {
    const parametroType = parametro.getType();
    const typeSymbol = parametroType.getSymbol();
    const declarations = typeSymbol?.getDeclarations();
    const interfaces = [];
    if (declarations) {
      for (const declaration of declarations) {
        if (declaration instanceof InterfaceDeclaration) {
          interfaces.push(declaration);
        }
      }
    }
    return interfaces;
  }

  public findInterfazProperty(
    parametro: PropertyDeclaration |PropertySignature
  ): InterfaceDeclaration | undefined {
    const interfaces = this.findInterfacesPropiedades(parametro);
    for (const interfaz of interfaces) {
      if (
        interfaz.getName() === parametro.getType().getSymbol()?.getEscapedName()
      ) {
        return interfaz;
      }
    }
    return undefined;
  }
}
