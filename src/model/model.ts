import { SourceProject } from "./SourceProject";

export interface TestGeneratorContext {
    tsconfigPath: string,
    testingFW: string,
    projectExclussion: string,
    sourceProject: SourceProject
  }
  