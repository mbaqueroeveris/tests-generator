// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import { SourceProject } from './model/SourceProject';
import { TestGeneratorContext } from './model/model';
import { generateTest, generateTestForSelection } from './utils/testGenerator';


let tgContextPromise:Promise<TestGeneratorContext>;
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	console.log('Congratulations, Test-Generator is now active!');	
	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	
	tgContextPromise = generateContext();

	context.subscriptions.push(vscode.commands.registerCommand('ts-test-generator.testGenerator', uri => generateTestsForCurrentfile(uri)));
	context.subscriptions.push(vscode.commands.registerCommand('ts-test-generator.testGeneratorFolder', uri => generateTestsForCurrentFolder(uri)));
	context.subscriptions.push(vscode.commands.registerCommand('ts-test-generator.testGeneratorAll', generateTestsForAllFiles));
	context.subscriptions.push(vscode.commands.registerCommand('ts-test-generator.testGeneratorSelection', uri => generateTestsForSelection(uri)));
}

// This method is called when your extension is deactivated
export function deactivate() {}

async function generateTestsForCurrentfile(uri:any) {
	
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
		let message:string;

		const tgContext = await tgContextPromise;
		//From contextual menu on active editor OR command palette -> uri not util OR Has no uri!! 
		if( vscode.workspace.workspaceFolders !== undefined &&
			vscode.window.activeTextEditor?.document?.fileName !== undefined &&
			(uri?.fsPath === undefined || uri?.fsPath === vscode.window.activeTextEditor?.document?.fileName) ) {
				
			let wf = vscode.workspace.workspaceFolders[0].uri.path ;
			let f = vscode.workspace.workspaceFolders[0].uri.fsPath ;
			let fileName = vscode.window.activeTextEditor?.document?.fileName;
		
			message = `Test Generator: folder: \n${wf}\n${f}` ;
			
			vscode.window.showInformationMessage(message);
			vscode.window.showInformationMessage(`Reading - ${fileName}`);

			if (fileName) {
				//Setting up a context object
				
				generateTestsForFile(fileName, tgContext);			
			}
		//From contextual menu on explorer -> Has uri (active editor must NOT compare)
		} else if(uri && uri.fsPath){

			let fileName = uri.fsPath;
			vscode.window.showInformationMessage(`Reading - ${fileName}`);

			//Setting up a context object
			
			generateTestsForFile(fileName, tgContext);			
		}
		else {
			message = "Test Generator: Working folder not found, open a folder/file and try again" ;
		
			vscode.window.showErrorMessage(message);
		}
}

async function generateTestsForCurrentFolder(uri:any) {
	
	// The code you place here will be executed every time your command is executed
	// Display a message box to the user
	let message:string;

	const tgContext = await tgContextPromise;
	//From contextual menu on active editor OR command palette -> uri not util OR Has no uri!! 
	if( vscode.workspace.workspaceFolders !== undefined &&
		vscode.window.activeTextEditor?.document?.fileName !== undefined &&
		(uri?.fsPath === undefined || uri?.fsPath === vscode.window.activeTextEditor?.document?.fileName) ) {
			
		let wf = vscode.workspace.workspaceFolders[0].uri.path ;
		let f = vscode.workspace.workspaceFolders[0].uri.fsPath ;
		let folderName = vscode.window.activeTextEditor?.document?.fileName;
	
		message = `Test Generator: folder: \n${wf}\n${f}` ;
		
		vscode.window.showInformationMessage(message);
		vscode.window.showInformationMessage(`Reading - ${folderName}`);

		if (folderName) {
			//Setting up a context object
			let proyectInclussion : vscode.GlobPattern = folderName + '/**/*.ts';
			let proyectExclussion : vscode.GlobPattern =  '{**/node_modules/**, **/dist/**, **/.git/**, **/coverage/**}';

			//Setting up a context object
			vscode.workspace.findFiles(proyectInclussion, proyectExclussion).then((uris: vscode.Uri[] ) => {
			
				uris.forEach((uri: vscode.Uri) => { 
					generateTestsForFile(uri.fsPath, tgContext);
				});
		
			}, (error => {console.log(error);}));
				
			}
	//From contextual menu on explorer -> Has uri (active editor must NOT compare)
	} else if(uri && uri.path){
		const wsfolder = vscode.workspace.getWorkspaceFolder(uri);
		let folderName = uri.path.replace(wsfolder?.uri.path, '');
		vscode.window.showInformationMessage(`Reading - ${folderName}`);

		let proyectInclussion : vscode.GlobPattern = `{**${folderName}/**/*.ts,**${folderName}/*.ts}`;
		let proyectExclussion : vscode.GlobPattern =  '{**/node_modules/**,**/dist/**,**/.git/**,**/coverage/**}';
		
		vscode.workspace.findFiles(proyectInclussion, proyectExclussion).then((uris: vscode.Uri[] ) => {
			if (!uris?.length) {
				vscode.window.showErrorMessage(`No files found with pattern: ${folderName}/**/*.ts`);
			} else {
				uris.forEach((uri: vscode.Uri) => {
					generateTestsForFile(uri.fsPath, tgContext);
				});
			}
			
	
		}, (error => {console.log(error);}));
		
	}
	else {
		message = "Test Generator: Working folder not found, open a folder/file and try again" ;
	
		vscode.window.showErrorMessage(message);
	}
}

async function generateContext(exclussions = '{**/node_modules/**, **/dist/**, **/.git/**, **/coverage/**}'): Promise<TestGeneratorContext> {
	let projectExclussion : vscode.GlobPattern =  exclussions;
	const tsconfigPath = (await findTsConfig())?.path;
	console.log('tsconfigPath: ', tsconfigPath);
	return {
		tsconfigPath,
		testingFW: await getTestingFramework(projectExclussion),
		projectExclussion,
		sourceProject: new SourceProject(tsconfigPath)
	};
}

async function generateTestsForAllFiles() {
	
	let proyectInclussion : vscode.GlobPattern = '**/src/**/*.ts';
	let proyectExclussion : vscode.GlobPattern =  '{**/node_modules/**, **/dist/**, **/.git/**, **/coverage/**}';

	//Setting up a context object
	const tgContext = await tgContextPromise;
	
	vscode.workspace.findFiles(proyectInclussion, proyectExclussion).then((uris: vscode.Uri[] ) => {
		
		uris.forEach((uri: vscode.Uri) => { 
			generateTestsForFile(uri.fsPath, tgContext);
		});

	}, (error => {console.log(error);}));

}


function generateTestsForFile(fileName:string, tgContext: TestGeneratorContext) {
	if (fileName) {
		let output = fileName.replace(/\.ts$/, '.gen.test.ts');
		
		try {
			generateTest(fileName, output, tgContext);
			vscode.window.showInformationMessage(`Output - ${output}`);
		} catch(err: any) {
			console.log("Error generando: " + err);
			vscode.window.showErrorMessage(`Test Generator: Error generating ${output}: ${err}`);
		}
		
	} else {
		vscode.window.showErrorMessage('This command must be used on a file');
	}
}

async function generateTestsForSelection(uri:any){
	if (uri?.fsPath) {
		//Setting up a context object
		const tgContext = await tgContextPromise;

		let fileName = uri.fsPath;
		let output = fileName.replace(/\.ts$/, '.gen.test.ts');
		
		try {
			let selection = vscode.window.activeTextEditor?.selection;
			if (selection) {
				const selectionRange = new vscode.Range(selection.start.line, selection.start.character, selection.end.line, selection.end.character);
				let selected = vscode.window.activeTextEditor?.document.getText(selectionRange) ?? '';
				selected = selected.trim();
	
				let fileName = vscode.window.activeTextEditor?.document?.fileName;
	
				console.log('selected text: ', selected);
				if (fileName) {
					generateTestForSelection(fileName, selected, output, tgContext);
					vscode.window.showInformationMessage(`Output - ${output}`);
				}
			}
			
		} catch(err: any) {
			console.log("Error generando: " + err);
			vscode.window.showErrorMessage(`Test Generator: Error generating ${output}: ${err}`);
		}
		
	} else {
		vscode.window.showErrorMessage('This command must be used on a file and selection.');
	}
}

async function getTestingFramework(proyectExclussion: any = null) {

	let pkg: any = vscode.workspace.findFiles('**/package.json', proyectExclussion)
	.then( ( (value: vscode.Uri[]) => vscode.workspace.fs.readFile(value[0]) ))
	.then( buff => JSON.parse(buff.toString()) );

	const prDeps = Object.keys((await pkg).devDependencies);
	let testingFW = 'jasmine';

	let depsFound = prDeps.some((key: string | string[]) => key.includes('jest'));

	if (depsFound) {
		testingFW = 'jest';
	} else {
		depsFound = prDeps.some((key: string | string[]) => key.includes('jasmine'));
		if (!depsFound) {
			vscode.window.showInformationMessage(`No testing framework detected, using jasmine by default.`);
		}
	}
	return testingFW;
}

async function findTsConfig(){
	const tsconfigPaths:vscode.Uri[] = await vscode.workspace.findFiles('tsconfig.json', null, 1);
	return tsconfigPaths?.[0];
}
