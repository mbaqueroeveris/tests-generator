import { CallExpression, ClassDeclaration, FunctionDeclaration, MethodDeclaration, ts } from "ts-morph";
import { TestGeneratorContext } from "../model/model";
import { TestSyntaxItem, spySyntaxItems } from "../utils/constants";

import { getVariableName } from "../utils/funcionesAst";
import { SampleValueGenerator } from "./SampleValueGenerator";

export class SpyOnGenerator {
  private sampleValueGenerator = new SampleValueGenerator();

  public getSpyOnForMethod(metodo: MethodDeclaration | FunctionDeclaration, tgcontent: TestGeneratorContext, containingClass?: ClassDeclaration): string[] {
    let resultado: string[] = [];

    //resultado.push(...getResultsOfChild(metodo.getBody()));

    const body = metodo.getBodyOrThrow();

    const testingSyntax = spySyntaxItems[tgcontent.testingFW];
    const spyOn: string = testingSyntax.spyOn;
    const variableName: string = getVariableName(containingClass);

    // Find all call expressions inside the method body
    const callExpressions = body.getDescendantsOfKind(ts.SyntaxKind.CallExpression);

    callExpressions.forEach((callExpr) => {
      if (this.isThisServiceMethodCall(callExpr)) {
        const callExprVars = callExpr.getText().replace(/[\r\n ]/g,'').split(".");
        const serviceName = callExprVars[1];
        const methodCall = callExprVars[2].replace(/\([\S\s]*/, "");
        const serviceProperty = containingClass?.getProperty(serviceName) || containingClass?.getConstructors()[0].getParameter(serviceName);
        const isPrivate = serviceProperty?.hasModifier(ts.SyntaxKind.PrivateKeyword);
        const returnType = callExpr.getReturnType();
        
        let mock = this.sampleValueGenerator.getSampleValueForType(returnType, callExpr);

        //${spyOn}(${spy}).${sintaxReturn(metodo, tgcontent)}(mock);\n
        if (isPrivate) {
          resultado.push(
            `${spyOn}(${variableName}['${serviceName}'], '${methodCall}').${this.sintaxReturn(testingSyntax, mock)};`
          );
        } else {
          resultado.push(`${spyOn}(${variableName}.${serviceName}, '${methodCall}').${this.sintaxReturn(testingSyntax, mock)};`);
        }
      }
    });

    return resultado;
  }

  public isThisServiceMethodCall(callExpr: CallExpression): boolean {
    const expression = callExpr.getExpression();

    // Check if it's a property access chain like 'this.service.methodCall'
    if (expression.getKind() === ts.SyntaxKind.PropertyAccessExpression) {
      const propertyAccess = expression.asKindOrThrow(ts.SyntaxKind.PropertyAccessExpression);

      const leftSide = propertyAccess.getExpression();
      // Check if left side is 'this.service'
      if (leftSide.getKind() === ts.SyntaxKind.PropertyAccessExpression) {
        const leftPropertyAccess = leftSide.asKindOrThrow(ts.SyntaxKind.PropertyAccessExpression);
        const exprtype = leftPropertyAccess.getType();
        const leftMostPart = leftPropertyAccess.getExpression();
        // Whitelist, do not mock base types
        if (exprtype.isString() || exprtype.isArray() || exprtype.isNumber() || exprtype.isBoolean()) {
          return false;
        }
        if (leftMostPart.getKind() === ts.SyntaxKind.ThisKeyword) {
          return true; // Found the pattern 'this.service.methodCall'
        }
      }
    }

    return false;
  }

  private sintaxReturn(testingSyntax: TestSyntaxItem, mock: string): string {
    if (mock.startsWith('Promise.resolve(')) {
      return `${testingSyntax.mockResolvedValue}${mock.replace('Promise.resolve', '')}`;
    }
    return `${testingSyntax.mockReturnValue}(${mock})`;
  }
}
