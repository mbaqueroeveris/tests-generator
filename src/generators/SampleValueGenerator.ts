import { ClassDeclaration, Node, Type, ts } from "ts-morph";

export class SampleValueGenerator {
  public getSampleValueForType(propType: Type<ts.Type>, callExpr: Node<ts.Node>): string {

    

    // It's a primitive type
    let sampleValue = this.getSampleValueForPrimitiveType(propType);

    if (!sampleValue && propType.isUnionOrIntersection()) {
      // Generate value for the first type
      return this.getSampleValueForType(propType.getUnionTypes()[0], callExpr);
    }

    if (!sampleValue) {
      // Check if it's a javascript object base type
      sampleValue = this.getSampleValueForBaseType(propType, callExpr);
    }

    if (!sampleValue) {
      // It's an interface or a type alias
      sampleValue = this.getSampleValueForLambdaType(propType, callExpr);
    }

    if (!sampleValue) {
      // Check if it's a class
      sampleValue = this.getConstructorForClass(propType);
    }

    if (!sampleValue) {
      // It's an interface or a type alias
      sampleValue = this.getSampleValueForInterfaceOrTypeAlias(propType, callExpr);
    }

    if (!sampleValue) {
      sampleValue = `/* instance of ${propType.getText()} */ undefined`;
    }

    return sampleValue;
  }

  public getSampleValueForPrimitiveType(propType: Type<ts.Type>): string | undefined {
    if (propType.isArray()) {
      return "[]";
    }
    
    if (propType.isString()) {
      return `'str'`;
    }
    if (propType.isNumber()) {
      return "123";
    }
    if (propType.isBoolean()) {
      return "true";
    }
    if (propType.isVoid() || propType.isUndefined() || propType.isNever()) {
      return "undefined";
    }
    if (propType.isNull()) {
      return "null";
    }
    if (propType.isAny() || propType.isUnknown()) {
      return "{}";
    }
    
    return undefined;
  }

  public getSampleValueForBaseType(propType: Type<ts.Type>, callExpr: Node<ts.Node>): string | undefined {
    const typeName = propType.getText();
    const symbol = propType.getSymbol();
    const aliasName = propType.getAliasSymbol()?.getName() || symbol?.getEscapedName();
    if (typeName === "Function") {
      return "() => {}";
    }
    if (typeName === "Date") {
      return "new Date()";
    }
    if (typeName === "Error") {
      return "new Error()";
    }
    if (typeName === "RegExp") {
      return "/.*/";
    }
    if (typeName === "Set") {
      return "new Set()";
    }
    if (aliasName === "Promise") {
      const resolvedArguments = propType
        .getAliasTypeArguments()
        ?.map((t) => this.getSampleValueForType(t, callExpr))
        .join(", ");
      return `Promise.resolve(${resolvedArguments || "undefined"})`;
    }
    if (aliasName === "Signal") {
      return `signal(${propType
        .getAliasTypeArguments()
        ?.map((t) => this.getSampleValueForType(t, callExpr))
        .join(", ")})`;
    }
    return undefined;
  }

  public getSampleValueForLambdaType(propType: Type<ts.Type>, callExpr: Node<ts.Node>): string | undefined {
    
    if (propType.isAnonymous()) {
      const returnType =propType.getCallSignatures()?.[0]?.getReturnType();
      if (returnType) {
        return "() => { return " + this.getSampleValueForType(returnType, callExpr) + "; }";
      }
    }
    
    return undefined;
  }

  getConstructorForClass(propType: Type<ts.Type>): string | undefined {
    const symbol = propType.getSymbol();
    if (symbol) {
      const classDeclaration = symbol.getDeclarations().find((d) => d instanceof ClassDeclaration) as ClassDeclaration | undefined;
      if (classDeclaration) {
        const className = classDeclaration.getName();
        if (classDeclaration && classDeclaration.isAbstract()) {
          console.log('abstract class');
          return `{} as ${className}`;
        } else {
          // It's a class, we can create an instance
          const firstConstructor = classDeclaration.getConstructors()[0];
          const params: string[] = [];
          firstConstructor.getParameters().forEach((p) => {
            params.push(this.getSampleValueForType(p.getType(), firstConstructor));
          });
          if (className === 'Observable') {
            return `of(${params.join(',')})`;
          }
          return `new ${className}(${params.join(',')})`;
        }
      }
    }
    return undefined;
  }

  getSampleValueForInterfaceOrTypeAlias(propType: Type<ts.Type>, callExpr: Node<ts.Node>): string | undefined {
    const properties = propType.getProperties();
    if (properties.length > 0) {
      let mock = "{";
      properties.forEach((property) => {
        const propName = property.getName();
        const propType = property.getTypeAtLocation(callExpr);
        mock += `  ${propName}: ${this.getSampleValueForType(propType, callExpr)},`;
      });
      mock += "}";
      return mock;
    }
    return undefined;
  }
}
