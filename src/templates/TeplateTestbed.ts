import { Template } from "./Teplate";

export class TemplateTestbed implements Template {
  public isAsinc: boolean = false;
  public declarations: string[] = [];
  public imports: string[] = [];
  public providers: string[] = [];
  public injects: string = "";
  public source: string = "";

  public getTemplate() {
    if (!this.source) {
      return '';
    }
    this.declarations.unshift('{{testing-file}}');
    this.isAsinc = true;
    return `
        await TestBed.configureTestingModule({
        declarations: [${this.declarations.join(',')}],
        imports: [CommonModule, ${this.imports.join(',')}],
        providers: [${this.providers.join(',')}],
        schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      }).compileComponents();
      ${this.injects}
      ${this.source}`;
  }
}
