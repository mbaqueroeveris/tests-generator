import { Template } from "./Teplate";

export class TemplateBeforeEach implements Template {
    public isAsinc: boolean = false;
    public source: string = '';
    
   public getTemplate() {
    if (!this.source) {
        return '';
    }
    return `
        beforeEach (${this.isAsinc ? 'async' : ''}() => {
            ${this.source}
        });`;
   } 
}