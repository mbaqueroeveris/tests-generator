import { Template } from "./Teplate";

export class TemplateBeforeAll implements Template {
    public isAsinc: boolean = false;
    public source: string = '';
    
   public getTemplate() {
    if (!this.source) {
        return '';
    }
    return `
        beforeAll (${this.isAsinc ? 'async' : ''}() => {
            ${this.source}
        });`;
   } 
}