import { Template } from "./Teplate";
import { TemplateBeforeAll } from "./TeplateBeforeAll";
import { TemplateBeforeEach } from "./TeplateBeforeEach";
import { TemplateIt } from "./TeplateIt";

export class TeplateDescribe implements Template {
  public imports: string = "";
  public className: string = "";
  public commonMocks: string = "";
  public beforeAll: TemplateBeforeAll[] = [];
  public beforeEach: TemplateBeforeEach[] = [];
  public tests: TemplateIt[] = [];

  public getTemplate() {
    return `
    ${this.imports}
    describe('Tests for class ${this.className}', () => {

        ${this.commonMocks}
        ${this.printTemplates(this.beforeAll)}
        ${this.printTemplates(this.beforeEach)}
        ${this.printTemplates(this.tests)}
    });`.replaceAll('{{testing-file}}', this.className);
  }

  private printTemplates(templates: Template[]|undefined):string {
    if (!templates) {
        return '';
    }
    return templates.map(template => template.getTemplate()).join('\n\n');
  }

}
