import { Template } from "./Teplate";

export class TemplateIt implements Template {
    public isAsinc: boolean = false;
    public testName: string = '';
    public source: string = '';
    
   public getTemplate() {
    if (!this.testName) {
        return '';
    }
    return `
        it ('${this.testName}', ${this.isAsinc ? 'async' : ''}() => {
            try{
                ${this.source}
            } catch (err){
                console.warn('Error in test ${this.testName}', err);
            }
        });`;
   } 
}