# Change Log

All notable changes to the "ts-test-generator" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.0.14]

- Bugfix component name, use instead variable class name

## [1.0.1 - 1.0.13]

- Multiple bugfixes to generate tests

## [1.0.0]

- Beta project to attempt test generation in components
